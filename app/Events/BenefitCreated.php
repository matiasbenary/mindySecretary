<?php

namespace App\Events;

use App\Models\Benefit;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BenefitCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Benefit
     */
    public $benefit;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Benefit $benefit)
    {
        //
        $this->benefit=$benefit;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
