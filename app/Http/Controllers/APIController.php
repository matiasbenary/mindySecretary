<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;

class APIController extends Controller
{
    //set entity_name as the entity that uses the Controller
    //set edit_view as the view to which the edit method redirects
    protected  $entity_name;
    protected $edit_view_name;
    protected $create_view_name;
    protected $list_view_name;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clase = $this->entity_name;
        $entity = new $clase();
        $data = $entity->get();
        return view($this->list_view_name,['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clase = $this->entity_name;
        $entity = new $clase();
        return view($this->create_view_name,['entity'=>$entity]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $data = new $clase(
                    $request->all()
                );
                $data->save();
                return response()->json(['status' => true, 'Guardado satisfactorio'], 200);
            } catch (Exception $e) {
                Log::critical("No se pudo guardar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $entity = $clase::find($id);
                if (!$entity) {
                    return response()->json(['No existe la entidad'], 404);
                }
                return response()->json($entity, 200);
            } catch (Exception $e) {
                Log::critical("No se pudo cargar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($this->entity_name) {
            $clase = $this->entity_name;
            $entity = $clase::find($id);
            if (!$entity) {
                $entity = new  $this->entity_name();
                $entity->user_id = $id;
            }
            return View($this->edit_view_name, ['entity' => $entity]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $entity = $clase::find($id);
                $entity->fill($request->all());
                $entity->save();
                return response()->json($entity, 200);
            } catch (Exception $e) {
                Log::critical("No se pudo actualizar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $entity = $clase::find($id);
                if (!$entity) {
                    return response()->json(['no se encontro la entidad'], 404);
                }
                $entity->delete();
            } catch (Exception $e) {
                Log::critical("No se pudo eliminar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

    public function setEntity_name($entity){
        $this->entity_name = $entity;
    }
    public function setEdit_view($editviewname){
        $this->edit_view_name = $editviewname;
    }
    public function setCreate_view($createviewname){
        $this->create_view_name = $createviewname;
    }
    public function setList_view($listviewname){
        $this->list_view_name = $listviewname;
    }
}
