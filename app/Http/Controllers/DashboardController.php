<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    //
    public function test(){
      return  Auth::user()->employee()->with('benefits')->get();
    }
}
