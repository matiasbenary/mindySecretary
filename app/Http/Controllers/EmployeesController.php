<?php

namespace App\Http\Controllers;

use App\Http\Controllers\APIController;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Event;
use App\Models\Sector;
use App\Models\User;
use App\Models\Shift;
use App\Models\Notification;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Mockery\Exception;
use function MongoDB\BSON\toJSON;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class EmployeesController extends APIController
{
    public function __construct(){
        //set entity_name as the entity that uses the Controller
        //set edit_view as the view to which the edit method redirects
        $this->setEntity_name('App\Models\Employee');
        $this->setCreate_view('Employees.create_employee');
        $this->setEdit_view('Employees.edit_employee');
        $this->setList_view('Employees.list');
    }
    public function get_count_employees(){
        //$array  = DB::table('employees')->selectRaw('id_sector name, count(*) y')->join('sectors','sectors.id','=','employees.id_sector')->groupBy('employees.id_sector')->get();
        $array  = DB::table('employees')->selectRaw('id_sector, count(*) y,sectors.descripcion name')
            ->join('sectors','sectors.id','=','employees.id_sector')
            ->groupBy('employees.id_sector')
            ->groupBy('sectors.descripcion')
            ->get();
        return response()->json($array, 200);
    }

       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clase = $this->entity_name;
        $entity = new $clase();
    /*$data = DB::table('employees')
        ->join('sectors','sectors.id','=','employees.id_sector')
        ->where('employees.status','<>','no-activo')
        ->get();*/

        $employees = $entity->where('status','<>','no-activo')->get();
        foreach($employees as $employee){
            $id_sector = $employee->id_sector;           
            $sector =  Sector::where('id','=',$id_sector)->first(); 
            if($sector){
                $employee->id_sector = $sector->descripcion;
            }else{
                $employee->id_sector = "";
            }
        }
        return view($this->list_view_name,['data'=>$employees]);
    }


      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clase = $this->entity_name;
        $entity = new $clase();
        return view($this->create_view_name,['entity'=>$entity,'sectors'=>Sector::all()]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($this->entity_name) {
            $clase = $this->entity_name;
            $entity = $clase::find($id);
            if (!$entity) {
                $entity = new  $this->entity_name();
                $entity->user_id = $id;
            }            
            $entity->email = User::find($entity->user_id)->email;
            return View($this->edit_view_name, ['entity' => $entity,'sectors'=>Sector::all()]);
        }
    }



    public function store(Request $request)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                //inicio inserta empleado
                //$empleado = Employee::find($request->get('dni'));
                $empleado = new Employee();
                $empleado->fill($request->all());
                $empleado->week_id  = 22;

            //    $id_jefe =DB::table('employee_sector')->where('sector_id','=',$request->get('id_sector'))->pluck('employee_id');                
              //  $empleado->id_jefe =$id_jefe[0];
                $id_jefe = DB::table('employee_sector')->where('sector_id','=',$empleado->id_sector)->first();  
                $empleado->id_jefe = $id_jefe?$id_jefe->employee_id:$empleado->id;
                $empleado->save();
                //fin inserta empleado

                //inicio inserta usuario
                $email = $request->get('email');
                $pass = rand(1000,99999);
                $password = bcrypt($pass);
                $user = new User();
                $user->name =  substr($request->get("nombre"),0,1). $request->get("apellido");//"jbaez"
                $user->email = $email;
                $user->password = $password;
                $user->save();
                //fin inserta usuario


                $empleado->user_id = $user->id;
                $empleado->save();
                return response()->json(['usuario' =>$user->email,'password'=>$pass,'status' => true, 'Guardado satisfactorio'], 200);
            } catch (Exception $e) {
                Log::critical("No se pudo guardar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }
      /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->entity_name) {
            try {
                //se guarda el empleado
                $clase = $this->entity_name;
                $entity = $clase::find($id);
                $entity->fill($request->all());
                $entity->save();

                //se edita el usuario en base al nombre 
                $email = $request->get("email");                
                $user = User::find($entity->user_id);
                $user->name = substr($request->get("nombre"),0). " " . $request->get("apellido");
                $user->email = $email;
                $user->save();

                return response()->json($entity, 200);
            } catch (Exception $e) {
                Log::critical("No se pudo actualizar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $entity = $clase::find($id);
                if (!$entity) {
                    return response()->json(['no se encontro la entidad'], 404);
                }
                $entity->status = "no-activo";
                $entity->save();
            } catch (Exception $e) {
                Log::critical("No se pudo eliminar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

    public function detail($user_id){
        $employee = Employee::where('user_id','=',$user_id)->first();
        return response()->json($employee);
    }


    public function getBirthDays($month){
        $employees = Employee::whereMonth('fecha_nacimiento','=',$month)->get();
        return response()->json(['employees' => $employees]);
    }

    public function getBirthDays2(){
      $employeess= Employee::whereDay('fecha_nacimiento', '=', Carbon::now()->toDayDate2TimeString())->whereMonth('fecha_nacimiento', '=', Carbon::now()->toDayDate3TimeString())->get();

      return response()->json(['employeess' => $employeess]);
    }

    public function getRotaEmpleado(){
        $empleado = Employee::where('user_id','=',Auth::user()->getAuthIdentifier())->first();
        $rota = Shift::find($empleado->shift);        
        return response()->json($rota);
    }

    public function getNotifications(){
        $empleado = Auth::user()->employee->id;

        $eventos=Event::selectRaw('count(*) as cantidad,benefits.nombre,events.estado')
            ->where('employee_id',$empleado)
            ->join('benefits','benefits.id','=','events.benefit_id')
            ->groupBy('benefits.nombre')
            ->whereIn('events.estado',["Aprobado","Desaprobado"])
            ->groupBy('events.estado')
            ->get()->toArray();
//
//        $aprobadas = DB::table('notifications')
//        ->select(DB::raw("count(*) as cantidad,benefits.nombre,events.estado"))
//        ->join("events",'event_id','=','events.id')
//        ->join('benefits','events.benefit_id','=','benefits.id')
//        ->where('employee_id','=',$empleado->id)
//        ->where('events.estado','=',"Aprobado")
//        ->groupBy('benefits.nombre','events.estado')
//        ->get();
//
//
//        $desaprobadas = DB::table('notifications')
//        ->select(DB::raw("count(*) as cantidad,benefits.nombre,events.estado"))
//        ->join("events",'event_id','=','events.id')
//        ->join('benefits','events.benefit_id','=','benefits.id')
//        ->where('employee_id','=',$empleado->id)
//        ->where('events.estado','=',"Desaprobado")
//        ->groupBy('benefits.nombre','events.estado')
//        ->get();

//SELECT COUNT(*),benefits.nombre FROM notifications JOIN events on event_id = events.id JOIN benefits on events.benefit_id = benefits.id GROUP BY benefits.nombre
        return response()->json(['eventos'=>$eventos]);
    }
};