<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Event;
use App\Models\Employee;
use App\Models\Benefit;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    
    $employee = Employee::find(Auth::User()->id);

    $events = $employee->event_true;
        \Log::info($events->toArray());

    foreach($events as $event){
        $benefit = Benefit::find($event->benefit_id);
        $event->title = $benefit->nombre;
    }
    return response()->json($events);
            
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function event_all()
    {
        $events = Auth::User()->employee->event()->with('benefit')->get();
        
        return response()->json($events);
    }

    public function eventForCalendar(){
        $events=Event::selectRaw('start,end,events.color,events.benefit_id as benefit_id,benefits.nombre as title,estado')
            ->join('benefits','benefits.id','=','events.benefit_id')
            ->where('estado', '=','aprobado')
            ->where('employee_id', '=',Auth::User()->employee->id)
            ->get();
        \Log::info($events->toArray());

        return response()->json($events);
    }

    

}
