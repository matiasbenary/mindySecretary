<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view();

    }

    public function role_switch(Request $request)
    {
        return $this->view($request);
    }

    protected function getEmployee(){

        return Auth::user()->employee()->with('benefits')->with('event.benefit')->first();
    }

    protected function view($request=null){
        $userType='user';
        $view='home';
        if(Auth::user()->hasRole('admin')){
            $userType=($request)?$request->get("usertype"):'admin';
        }
        $data=[];
        if($userType=='admin'){
            $view="home_2";
        }else{
            $data=$this->getEmployee();
        }
        return view($view,['empleado'=>$data,'usertype'=>$userType]);
    }

}
