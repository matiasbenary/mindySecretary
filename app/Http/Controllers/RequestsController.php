<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Request as Peticion;
use Auth;
use Illuminate\Support\Facades\DB;


class RequestsController extends Controller
{

    public function getCompanyRequestsCount(){
        //SELECT count(benefit_id),benefits.nombre FROM `events` JOIN benefits ON benefit_id = benefits.id group by benefit_id
        $requests = DB::table('events')
        ->join('benefits','benefit_id','=','benefits.id')
        ->select(DB::raw('count(benefit_id) AS cantidad'),'benefits.nombre')
        ->groupBy('benefit_id','benefits.nombre')
        ->get();
        return response()->json($requests);
    }

    public function getCompanyRequestStatusCount(){
        //SELECT count(benefit_id),benefits.nombre FROM `events` JOIN benefits ON benefit_id = benefits.id group by benefit_id
        $requests = DB::table('events')
        ->select(DB::raw('count(benefit_id) AS cantidad'),'estado as estado')
        ->groupBy('estado','estado')
        ->get();
        return response()->json($requests);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Peticion::where('employee_id',Auth::user()->employee->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $peticion=new Peticion();
        $data=($request->except('_token'));
        foreach ($data as $key=>$datum){
            $peticion->$key=$datum;
        }
        $peticion->save();
        return  response()->json(['status' => true, 'Guardado satisfactorio'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)

    {
        $peticiones = Peticion::where('id',$id)->with('benefit');

        return response()->json($peticiones, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
