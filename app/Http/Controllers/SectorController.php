<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Sector;
use App\Models\Employee;
use Auth;

class SectorController extends ApiController
{

    public function __construct(){
        $this->setEntity_name('App\Models\Sector');
        $this->setCreate_view('Sectors.create_sector');
        $this->setEdit_view('Sectors.edit_sector');
        $this->setList_view('Sectors.list');
    }

    //override
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectors = Sector::all();
        $list = array();
        foreach ($sectors as $sector) {
           $list2 = ['id'=>$sector->id,'descripcion' => $sector->descripcion, 'cantidad' => $sector->cantidad(), 'acargo' => $sector->incharge()->nombre . ' ' . $sector->incharge()->apellido];
           array_push($list,$list2);
        }
        
        return view($this->list_view_name,['sectors'=>json_encode($list)]);
    }

    public function store(Request $request)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $sector = new Sector();
                $sector->fill($request->all());
                $sector->save();

                $employees = Employee::all();
                foreach ($employees as $employee ) {
                    if ((int)$request->get('acargo') == $employee->dni) {
                        $employee->incharge = $sector->id;
                        $employee->save();
                    }

                }
            
            } catch (Exception $e) {
                Log::critical("No se pudo guardar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

    public function update(Request $request, $id)
    {
        if($this->entity_name) {
            try {
                $clase = $this->entity_name;
                $sector = $clase::find($id);
                $sector->fill($request->all());
                $sector->save();

                $oldincharge = Employee::find($sector->incharge()->id);
                $oldincharge->incharge = null;
                $oldincharge->save();

                $employees = Employee::all();
                foreach ($employees as $employee ) {
                    if ((int)$request->get('acargo') == $employee->dni) {
                        $employee->incharge = $sector->id;
                        $employee->save();
                    }

                }

                return response()->json([], 200);
            } catch (Exception $e) {
                Log::critical("No se pudo actualizar la informacion: {$e->getCode()},{$e->getLine()},{$e->getMessage()}");
            }
        }
    }

}
