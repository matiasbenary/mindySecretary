<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\APIController;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Mockery\Exception;
use function MongoDB\BSON\toJSON;

class UserdataController extends APIController
{
    public function __construct(){
        //set entity_name as the entity that uses the Controller
        //set edit_view as the view to which the edit method redirects
        $this->setEntity_name('App\Models\Employee');
        $this->setCreate_view('User\userdata');
        $this->setEdit_view('User\userdata');
    }
}
