<?php

namespace App\Listeners;

use App\Events\BenefitCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Employee;
use App\Models\Benefit;

class CreatedBenefitEmployee
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BenefitCreated  $event
     * @return void
     */
    public function handle(BenefitCreated $event)
    {
        Employee::whereHas('privileges',function ($query) use ($event){
            $query->where('privileges.id',$event->benefit->privilege_id);
        })->each(function ($empleado) use($event){
            $empleado->benefits()->attach($event->benefit->id,['cantidad'=>$event->benefit->cantidad]);
        });
    }
}
