<?php

namespace App\Listeners;

use App\Events\BenefitUpdating;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Cache;
use App\Models\Benefit;

class SaveChangeBenefit
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BenefitUpdating  $event
     * @return void
     */
    public function handle(BenefitUpdating $event)
    {
        //
        $beneficio=Benefit::select('cantidad','id')->where('id',$event->benefit->id)->first();
        Cache::forever("Benefit".$beneficio->id, ["cantidad"=>$beneficio->cantidad]);
    }
}
