<?php

namespace App\Listeners;

use App\Events\BenefitUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Cache;
use App\Models\Employee;
use DB;

class UpdatedBenefitEmployee
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BenefitUpdated  $event
     * @return void
     */
    public function handle(BenefitUpdated $event)
    {
        $id=$event->benefit->id;
        $cantidadAnterior=$this->getCantidadAnterior($id);
        $diffCantidad=$event->benefit->cantidad-$cantidadAnterior;
        if($diffCantidad!=0){
            $this->updateUser($id,$diffCantidad);
        }
        Cache::forget("Benefit".$id);
    }

    protected function getCantidadAnterior($id)
    {
        return Cache::get("Benefit".$id)['cantidad'];
    }

    protected function updateUser($id,$cantidad)
    {

        $empleados=Employee::whereHas('benefits',function ($query) use($id) {
            $query->where('benefit_id',$id);
        })
            ->with(['benefits'=>function ($query) use($id) {
                $query->where('benefits.id',$id);
            }])->get();

        DB::beginTransaction();
        foreach ($empleados as $empleado){
            $newCantidad=$empleado->benefits[0]->pivot->cantidad+$cantidad;
            if($newCantidad<0){
                $newCantidad=0;
            }
            $empleado->benefits()->updateExistingPivot($id,['cantidad'=>$newCantidad]);
        }
        DB::commit();
    }

}
