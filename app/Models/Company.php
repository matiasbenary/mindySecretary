<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Company
 *
 * @property int $id
 * @property string $nombre
 * @property string $forma_juridica
 * @property string $domicilio
 * @property int $telefono
 * @property int $numero_socios
 * @property int $capital_social
 * @property string $sector_actividad
 * @property string $descripcion
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereCapitalSocial($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereDescripcion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereDomicilio($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereFormaJuridica($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereNumeroSocios($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereSectorActividad($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereTelefono($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Company extends Model
{
    protected $table='companies';
    protected $primaryKey = 'id';

    protected $fillable = array('id','nombre','forma_juridica','domicilio','telefono',
        'numero_socios','capital_social','sector_actividad','descripcion');



}
