<?php
/**
 * Created by PhpStorm.
 * User: Baez
 * Date: 30/03/2017
 * Time: 19:51
 */

namespace App\Models;


use App\Events\EmployeePrivilegeCreated;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Employee
 *
 * @property int $id
 * @property int $dni
 * @property string $nombre
 * @property string $apellido
 * @property string $fecha_nacimiento
 * @property string $lugar_nacimiento
 * @property string $sexo
 * @property string $estado_civil
 * @property string $direccion
 * @property string $ciudad
 * @property string $provincia
 * @property int $codigo_postal
 * @property int $telefono
 * @property int $id_jefe
 * @property int $antiguedad
 * @property bool $is_student
 * @property int $user_id
 * @property int $id_sector
 * @property int $week_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Benefit[] $benefits
 * @property-read \App\Models\Sector $sector
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereAntiguedad($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereApellido($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereCiudad($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereCodigoPostal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereDireccion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereDni($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereEstadoCivil($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereFechaNacimiento($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereIdJefe($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereIdSector($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereIsStudent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereLugarNacimiento($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereProvincia($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereSexo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereTelefono($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Employee whereWeekId($value)
 * @mixin \Eloquent
 */
class Employee extends Model
{
    protected $fillable = array('id','dni','nombre','apellido','fecha_nacimiento','lugar_nacimiento','sexo',
        'estado_civil','direccion','ciudad','provincia','codigo_postal','telefono','fecha_ingreso_empresa','user_id','id_sector','id_jefe','antiguedad','status','is_student','shift','incharge');

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function sector(){
        return $this->hasOne('App\Models\Sector');
    }

//    public function request(){
//        return $this->hasMany('App\Models\Request');
//    }

    public function requestRRHH(){
        return $this->hasMany('App\Models\Request','responsible_id');
    }

    public function benefits(){
        return $this->belongsToMany('App\Models\Benefit')->withPivot('cantidad')->withTimestamps();
    }

    public function event(){
        return $this->hasMany('App\Models\Event');
    }

    public function event_true () {
        return $this->hasMany('App\Models\Event')->where('estado', '=','aprobado');
    }

    public function shift(){
        return $this->hasOne('App\Models\Shift','shift');        
    }

    public function notification(){
        return $this->belongsToMany('App\Models\Notification');       
    }

    public function privileges(){
        return $this->belongsToMany('App\Models\Privilege')->withTimestamps();
    }
}