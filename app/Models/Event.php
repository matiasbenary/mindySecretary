<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{    
    protected $primaryKey = 'id';

    protected $fillable = array('id','benefit_id','employee_id','responsible_id','title','filepath','start','end','color','estado','created_at','updated_at');

    public function employee(){
    	return $this->belongsTo('App\Models\Employee');
    }

    public function notification(){
    	return $this->belongsTo('App\Models\Notification');
    }

    public function employeeRRHH(){
        return $this->belongsTo('App\Models\Employee','responsible_id');
    }

    public function benefit()
    {
        return $this->belongsTo('App\Models\Benefit');
    }
}
