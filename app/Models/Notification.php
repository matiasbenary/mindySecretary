<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
     protected $primaryKey = 'id';

    protected $fillable = array('id','responsible_employee_id','event_id');

    public function responsible_employee(){
    	return $this->hasOne('App\Models\Employee');
    }
    public function employee(){
    	return $this->hasOne('App\Models\Employee');
    }
    public function event(){
        return $this->hasOne('App\Models\Event');
    }
}
