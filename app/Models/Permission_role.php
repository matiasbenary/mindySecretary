<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Permission_role
 *
 * @property int $id
 * @property int $permission_id
 * @property int $role_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_role wherePermissionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_role whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission_role extends Model
{
    protected $table = 'permission_role';
    protected $fillable = array('id', 'permission_id', 'role_id','created_at','updated_at');
}
