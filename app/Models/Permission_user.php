<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Permission_user
 *
 * @property int $id
 * @property int $permission_id
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_user whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_user whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_user wherePermissionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_user whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permission_user whereUserId($value)
 * @mixin \Eloquent
 */
class Permission_user extends Model
{
    protected $table = 'permission_user';
    protected $fillable = array('id', 'permission_id', 'role_id','created_at','updated_at');
}
