<?php

namespace App\Models;

use App\Events\EmployeePrivilegeCreated;
use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    protected $fillable = array('id', 'nombre', 'descripcion');

    public function employees()
    {
        return $this->belongsToMany('App\Models\Employee');
    }

    public function benefit()
    {
        return $this->hasMany('App\Models\Benefit');
    }

}
