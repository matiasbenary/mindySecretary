<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = array('id', 'employee_id', 'benefit_id','responsible_id','filepath','start_day','finish_day','status');

    public function employee(){
        return $this->belongsTo('App\Models\Employee');
    }

    public function employeeRRHH(){
        return $this->belongsTo('App\Models\Employee','responsible_id');
    }

    public function benefit(){
        return $this->belongsTo('App\Models\Benefit');
    }
}
