<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Rol
 *
 * @mixin \Eloquent
 */
class Rol extends Model
{
    protected $fillable = array('id', 'name', 'slug','descripcion','level','created_at','updated_at');
}
