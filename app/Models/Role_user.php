<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Role_user
 *
 * @property int $id
 * @property int $role_id
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role_user whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role_user whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role_user whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role_user whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role_user whereUserId($value)
 * @mixin \Eloquent
 */
class Role_user extends Model
{
    protected $table = 'role_user';
    
    protected $fillable = array('id','role_id','user_id','created_at','updated_at');
}
