<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Sector
 *
 * @property int $id
 * @property string $descripcion
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sector whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sector whereDescripcion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sector whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sector whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sector extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = array('id','descripcion');

   	public function incharge(){
   		foreach (Employee::all() as $employee) {
   			if ($employee->incharge == $this->id) {
   				return $employee;
   			}
   		}
      return new Employee();
   	}
   	
   	public function cantidad(){
    	$cantidad = 0;
        foreach (Employee::all() as $employee){
        	if ($employee->id_sector == $this->id) {
        		$cantidad = $cantidad + 1;
        	}
    	
    	}
    	return $cantidad;
	}
}
