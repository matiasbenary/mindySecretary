<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Shift
 *
 * @property int $id
 * @property string $descripcion
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Shift whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shift whereDescripcion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shift whereHoraFin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shift whereHoraInicio($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shift whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Shift whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Shift extends Model
{
     protected $primaryKey = 'id';

    protected $fillable = array('id','descripcion','hora_inicio','hora_fin');


    public function employee(){
   		return $this->belongsTo("App\Models\Employee");
   	}
}
