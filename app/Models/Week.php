<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Week
 *
 * @property int $id
 * @property string $descripcion
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Week whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Week whereDescripcion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Week whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Week whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Week extends Model
{
     protected $primaryKey = 'id';
    protected $fillable = array('id','Descripcion');

}
