<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\BenefitCreated' => [
            'App\Listeners\CreatedBenefitEmployee',
        ],
        'App\Events\BenefitUpdated' => [
            'App\Listeners\UpdatedBenefitEmployee',
        ],
        'App\Events\BenefitUpdating' => [
            'App\Listeners\SaveChangeBenefit',
        ],
//        'App\Events\BenefitDeleted' => [
//            'App\Listeners\DeletedBenefitEmployee',
//        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
