<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [

        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Week::class,function(Faker\Generator $faker)
{
    return [
        'descripcion' => $faker->dayOfWeek
    ];
});


$factory->define(App\Models\Benefit::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->word,
        'descripcion' => $faker->word,
        'cantidad' => $faker->randomNumber(),
    ];
});

$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
    $beneficioid = $faker->numberBetween(1,5);
    return [
        
        'title' => App\Models\Benefit::find($beneficioid)->nombre,
        'estado' => $faker->randomElement($array = array ('Pendiente','Aprobado','Desaprobado')),
        'benefit_id'=> $beneficioid,
        'start'=> $faker->dateTimeThisMonth(),
        'color'=> App\Models\Benefit::find($beneficioid)->color,

    ];
});

$factory->define(App\Models\Company::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'forma_juridica' => $faker->word,
        'domicilio' => $faker->word,
        'telefono' => $faker->randomNumber(),
        'numero_socios' => $faker->randomNumber(),
        'capital_social' => $faker->randomNumber(),
        'sector_actividad' => $faker->word,
        'descripcion' => $faker->word,
    ];
});

$factory->define(App\Models\Employee::class, function (Faker\Generator $faker) {
    return [ 
        'dni'=>$faker->numberBetween(20000000,35000000),
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'fecha_nacimiento' => $faker->date(),
        'lugar_nacimiento' => $faker->address,
        'sexo' => $faker->randomElement(['Hombre', 'Mujer']),
        'estado_civil' => $faker->randomElement(['Casado/a','Soltero/a','Divorciado/a',"Viudo/a"]),
        'direccion' => $faker->streetAddress,
        'ciudad' => $faker->city,
        'provincia' => $faker->citySuffix,
        'codigo_postal' => $faker->numberBetween(1000,3000),
        'telefono' => $faker->numberBetween(40000000,50000000),
        'id_sector' => $faker->numberBetween(1,5),
        'id_jefe' => $faker->numberBetween(1,50),
        'antiguedad' => $faker->randomNumber(),
        'fecha_ingreso_empresa' => $faker->date(),
        'user_id' => factory(App\Models\User::class)->create()->id,
        'week_id' => $faker->numberBetween(1,50),
        'status' => $faker->randomElement($array = array ('activo','no-activo')),
        'is_student'=>$faker->boolean(),
        'shift' => $faker->numberBetween(1,4)

    ];
});

$factory->define(App\Models\Sector::class, function (Faker\Generator $faker) {
    return [
        'descripcion' => $faker->randomElement(['Finanzas','Empresarial','RRHH','IT','Judicial']),
    ];
});

