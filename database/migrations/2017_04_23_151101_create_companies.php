<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies',function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->string('nombre',30);
            $table->string('forma_juridica',30);
            $table->string('domicilio',30);
            $table->integer('telefono');
            $table->integer('numero_socios');
            $table->bigInteger('capital_social');
            $table->string('sector_actividad',30);
            $table->string('descripcion',250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
