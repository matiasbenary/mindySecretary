<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('privilege_id')->unsigned();
            $table->string('nombre', 30);
            $table->string('descripcion', 250)->nullable();
            $table->integer('cantidad');
            $table->string('color')->nullable();
            $table->boolean('need_certificate');
            $table->boolean('only_notice');
            $table->boolean('only_one_day');
            $table->enum('prioridad',['alta','media','baja']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('benefits');
    }
}
