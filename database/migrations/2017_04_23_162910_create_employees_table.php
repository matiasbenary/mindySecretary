<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('dni')->unsigned();
            $table->string('nombre');
            $table->string('apellido');
            $table->date('fecha_nacimiento')->nullable();
            $table->string('lugar_nacimiento')->nullable();
            $table->string('sexo')->nullable();
            $table->string('estado_civil')->nullable();
            $table->string('direccion')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('provincia')->nullable();
            $table->integer('codigo_postal')->nullable();
            $table->integer('telefono')->nullable();
            $table->date('fecha_ingreso_empresa')->nullable();
            $table->integer('id_jefe');
            $table->integer('id_sector');
            $table->integer('antiguedad');
            $table->boolean('is_student')->default("0");
            $table->string('status')->default("activo");


            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('incharge')->unsigned()->index()->nullable();
            $table->integer('week_id')->unsigned()->index();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade');
            $table->foreign('incharge')->references('id')->on('sectors')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
