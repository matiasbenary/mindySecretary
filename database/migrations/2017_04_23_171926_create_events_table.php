<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('employee_id')->unsigned()->index();
            $table->integer('benefit_id')->unsigned()->index();
            $table->integer('responsible_id')->nullable()->unsigned()->index();
            $table->string('title', 250)->nullable();
            $table->date('start');
            $table->date('end')->nullable();
            $table->string('color');
            $table->string('filepath')->nullable();
            $table->enum('estado', ['Pendiente','Aprobado','Desaprobado','Falta Certificado'])->default('Pendiente');
            $table->foreign('benefit_id')->references('id')->on('benefits')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('responsible_id')->references('id')->on('employees')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
