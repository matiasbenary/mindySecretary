<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftWeekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_week', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('week_id')->unsigned()->index();
            $table->enum('day', ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo']);
            $table->integer('shift_id')->unsigned()->index();

            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade');
            
            $table->foreign('shift_id')->references('id')->on('shifts')->onDelete('cascade');
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift_week');
    }
}
