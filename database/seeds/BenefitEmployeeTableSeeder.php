<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 17/05/17
 * Time: 21:42
 */
use Illuminate\Database\Seeder;
use App\Models\Employee;

class BenefitEmployeeTableSeeder extends Seeder{
    //put your code here
    public function run()
    {
        foreach (Employee::all() as $empleado){
            $empleado->benefits()->attach(1,['cantidad'=>rand(0,20)]);
            $empleado->benefits()->attach(2,['cantidad'=>rand(0,20)]);
        }

    }
}