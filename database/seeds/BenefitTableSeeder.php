<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 17/05/17
 * Time: 21:42
 */
use Illuminate\Database\Seeder;
use App\Models\Benefit;

class BenefitTableSeeder extends Seeder{
    //put your code here
    public function run()
    {
        $vacaciones=new Benefit();
        $vacaciones->nombre = "vacaciones";
        $vacaciones->color = "#FF5733";
        $vacaciones->need_certificate = false;
        $vacaciones->only_notice = false;
        $vacaciones->only_one_day = false;
        $vacaciones->prioridad = 'baja';
        $vacaciones->privilege_id=2;
        $vacaciones->cantidad=20;
        $vacaciones->save();

        $franco=new Benefit();
        $franco->nombre = "franco";
        $franco->color = "#1D8348";
        $franco->need_certificate = false;
        $franco->only_one_day = true;
        $franco->only_notice = false;
        $franco->prioridad = 'media';
        $franco->privilege_id=2;
        $franco->cantidad=20;
        $franco->save();

        $diaEstudio=new Benefit();
        $diaEstudio->nombre = "dia de estudio";
        $diaEstudio->color = "#D4AC0D";
        $diaEstudio->need_certificate = true;
        $diaEstudio->only_notice = false;
        $diaEstudio->only_one_day = true;
        $diaEstudio->prioridad = 'baja';
        $diaEstudio->privilege_id=1;
        $diaEstudio->cantidad=20;
        $diaEstudio->save();

        $enfermedad=new Benefit();
        $enfermedad->nombre = "enfermedad";
        $enfermedad->color = "#1B4F72";
        $enfermedad->need_certificate = true;
        $enfermedad->only_notice = true;
        $enfermedad->only_one_day= true;
        $enfermedad->prioridad = 'alta';
        $enfermedad->privilege_id=2;
        $enfermedad->cantidad=20;
        $enfermedad->save();


        $cambiohorario=new Benefit();
        $cambiohorario->nombre = "cambio horario";
        $cambiohorario->color = "#B03A2E";
        $cambiohorario->need_certificate = false;
        $cambiohorario->only_notice = true;
        $cambiohorario->only_one_day= false;
        $cambiohorario->prioridad = 'alta';
        $cambiohorario->privilege_id=2;
        $cambiohorario->cantidad=20;
        $cambiohorario->save();
    }
}