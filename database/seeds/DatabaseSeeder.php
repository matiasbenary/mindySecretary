<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        
        factory(App\Models\Week::class, 50)->create();
        factory(App\Models\Company::class, 1)->create();
        Model::unguard();
        $this->call('SectorSeeder');
        $this->call(ShiftsSeeder::class);
        $this->call('EmployeeSeeder');
        $this->call('PrivilegeTableSeeder');
        $this->call('BenefitTableSeeder');
//        $this->call('BenefitEmployeeTableSeeder');
        $this->call(UserRolesSeeder::class);
        $this->call('EventSeeder');
        $this->call('NotificationSeeder');
        $this->call('InchargeSeeder');
        Model::reguard();
//        factory(App\Models\Event::class, 10)->create();
    }
}
