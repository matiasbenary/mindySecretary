<?php

use Illuminate\Database\Seeder;

use App\Models\Employee;
use App\Models\Sector;


class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	Employee::unguard();
    	$admin = array('dni'=>1,
        'nombre' => 'Jon',
        'apellido' => 'Salchichon',
        'fecha_nacimiento' => date("Y-m-d H:i:s"),
        'lugar_nacimiento' =>'alguna',
        'sexo' => array_rand(['Hombre', 'Mujer'],1),
        'estado_civil' =>array_rand(array('Casado/a','Soltero/a','Divorciado/a',"Viudo/a"),1),
        'direccion' =>'alguna',
        'ciudad' => 'alguna',
        'provincia' => 'alguna',
        'codigo_postal' => 1888,
        'telefono' => 123456789,
        'id_sector' => 2,
        'id_jefe' =>3,
        'antiguedad' => 7,
        'fecha_ingreso_empresa' => date("Y-m-d H:i:s"),
        'user_id' =>1,
        'week_id' => 5,
        'status' => array_rand(array('activo','no-activo'),1),
        'is_student'=>false,
        'shift' => 3);
        $db = DB::table('employees')->insert($admin);
        factory(App\Models\Employee::class, 50)->create();

        foreach (Sector::all() as $sector) {
            $employee = Employee::find(random_int(1, 50));
            $employee->incharge = $sector->id;
            
        }
    }
}
