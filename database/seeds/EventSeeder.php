<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Event;
use Carbon\Carbon;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startDate = Carbon::now();
        $endDate = Carbon::now()->addMonth(3);
        $admin = Employee::where('user_id', 1)->first();
        foreach (Employee::with('benefits')->get() as $empleado) {
            foreach ($empleado->benefits as $benefit) {
                if (rand(0, 100) > 30) {
                    $event = new Event();
                    $event->benefit_id = $benefit->id;
                    $event->employee_id = $empleado->id;
                    $event->start = Carbon::createFromTimestamp(rand($endDate->timestamp, $startDate->timestamp))->format('Y-m-d');
                    $event->color = $benefit->color;
                    if ($benefit->need_certificate) {
                        $event->estado = "Falta Certificado";
                    }
                    if (rand(0, 100) > 35) {
                        $event->responsible_id = $admin->id;
                        if (rand(0, 1)) {
                            $event->estado = "Aprobado";
                        } else {
                            $event->estado = "Desaprobado";
                        }
                        if ($benefit->need_certificate) {
                            $event->filepath = "Certificado";
                        }
                    }
                    $event->save();
                }
            }
        }
    }
}

