<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Sector;

class InchargeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Sector::all() as $sector) {
            $employee = Employee::find(random_int(1, 50));
            $employee->incharge = $sector->id;
            $employee->save();
            
    	}
	}
}