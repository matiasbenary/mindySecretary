<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Event;
use App\Models\Notification;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Employee::all() as $empleado){
        	
            foreach ($empleado->event as $event){

            	$notification = new Notification;
            	$notification ->event_id = $event ->id;
            	$notification ->responsible_employee_id = 1;
                $notification ->employee_id = $empleado ->id;
            	$notification ->save();

            }
           
        }
    }
}
