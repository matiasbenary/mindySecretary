<?php
/**
 * Created by PhpStorm.
 * User: matias
 * Date: 17/05/17
 * Time: 21:42
 */
use Illuminate\Database\Seeder;
use App\Models\Privilege;
use App\Models\Employee;

class PrivilegeTableSeeder extends Seeder{
    //put your code here
    public function run()
    {
        $estudiante=new Privilege();
        $estudiante->nombre="estudiante";
        $estudiante->descripcion="Es estudiante de facultad";
        $estudiante->save();

        $general=new Privilege();
        $general->nombre="general";
        $general->descripcion="se asigna a todos los empleados";
        $general->save();

        foreach (Employee::all() as $empleado) {
            $empleado->privileges()->attach($general);
            if(rand(0,10) > 7){
                $empleado->privileges()->attach($estudiante);
            }
        }
    }
}