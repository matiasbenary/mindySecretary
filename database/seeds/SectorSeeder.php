<?php

use Illuminate\Database\Seeder;
use App\Models\Sector;
use App\Models\Employee;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sector1 = new Sector();
        $sector1->descripcion = 'RRHH';
        $sector1->save();
        $sector2 = new Sector();
        $sector2->descripcion ='Finanzas';
        $sector2->save();
        $sector3 = new Sector();
        $sector3->descripcion = 'Contabilidad';
        $sector3->save();
        $sector4 = new Sector();
        $sector4->descripcion = 'Juridico';
        $sector4->save();
        $sector5 = new Sector();
        $sector5->descripcion = 'IID';
        $sector5->save();
    }
}
