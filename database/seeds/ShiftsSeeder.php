<?php
/**
 * Created by PhpStorm.
 * User: jbaez
 * Date: 08/06/17
 * Time: 10:42
 */
use Illuminate\Database\Seeder;
use App\Models\Shift;

class ShiftsSeeder extends Seeder{
    //put your code here
    public function run()
    {
        Shift::create([
            "id" => "1",
            "descripcion" => "Turno Administrativo",
            "hora_inicio"=>"09:00",
            "hora_fin"=>"18:00"
        ]);
        Shift::create([
            "id" => "2",
            "descripcion" => "Turno Mañana",
            "hora_inicio"=>"06:00",
            "hora_fin"=>"14:00"
        ]);
        Shift::create([
            "id" => "3",
            "descripcion" => "Turno Tarde",
            "hora_inicio"=>"14:00",
            "hora_fin"=>"22:00"
        ]);
        Shift::create([
            "id" => "4",
            "descripcion" => "Turno Noche",
            "hora_inicio"=>"22:00",
            "hora_fin"=>"06:00"
        ]);         
    }
}