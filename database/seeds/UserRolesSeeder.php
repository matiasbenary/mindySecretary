<?php

use Illuminate\Database\Seeder;
use HttpOz\Roles\Models\Role;
use App\Models\User;
class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administador=Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => '', // optional
            'group' => 'administrador'
        ]);

        Role::create([
            'name' => 'RRHH',
            'slug' => 'RRHH',
            'description' => '', // optional
            'group' => 'RRHH'
        ]);

        Role::create([
            'name' => 'Empleado',
            'slug' => 'empleado',
            'description' => '', // optional
            'group' => 'empleado'
        ]);


        $admin=User::where('email','admin@admin')->first();
        $admin->attachRole($administador);
    }
}
