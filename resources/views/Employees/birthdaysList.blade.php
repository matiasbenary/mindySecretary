@extends('layouts.app')

@section('content')
    <h3>Empleados que cumples en este mes</h3>
    <table id="birthdays" class="col-md-offset-2 display">
    </table>
    <form name="employeedata" action="/employees_info" method="GET">
        {{ csrf_field() }}
    </form>
<script>
    $(document).ready(function(){
        var employees = [];
        var url = "birthdays/" + ((new Date().getMonth())+1);
        console.log(url);
        $.ajax({url:url,data:{},method:'GET',async:false}).done(function(response){
            employees = response.employees;
            console.log(response);
        });
        //var dataset = [[1231,'jonathan','baez','adasd','','',''],[1231,'jonathan','baez','adasd','','','']];
        var table = $('#birthdays').DataTable(
            {
                data:employees,
                columnDefs: [
                    { }
                ],
                columns: [
                    { title: "DNI",data:"dni" },
                    { title: "Nombre",data:"nombre" },
                    { title: "Apellido",data:"apellido"  },
                    { title: "Fecha de nacimiento",data:"fecha_nacimiento"  }
                ],
                oLanguage: {
                "sSearch": "Buscar empleado: ",
                "oPaginate": {
                          "sPrevious": "Anterior",
                          "sNext": "Siguiente",
                        },
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ hasta _END_ de _TOTAL_",
                "sInfoEmpty": "Mostrando 0 hasta 0 de 0 resultados"
                },                
                bLengthChange:false
            }
        );
    });
</script>
@endsection