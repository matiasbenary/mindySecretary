@extends('layouts.app')
@section('content')
    <div id="msg" class="col-xs-12 alert" style="display:none" ></div>


    <h3>Nuevo empleado</h3>

    <button class="btn btn-primary" style="float:right" onclick="$('#usuarioCreado').modal()">Ver usuario creado</button>
    <form action="/employees_info" method="POST" onsubmit="saveData(event,this);">
        @include('Employees.employeeForm')

    </form>
	@foreach ($errors as $error)
   <h4>{{$error}}</h4>
	@endforeach

 <div class="modal fade" id="usuarioCreado" style="z-index:99999 ;" tabindex="-1" role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 style="display:inline" class="modal-title" id="exampleModalLongTitle">Usuario creado</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <b>Usuario: </b><p name="usuario"></p>
          <b>Constraseña: </b><p name="password"></p>
      </div>
      <div class="modal-footer"><button class="btn btn-default" data-dismiss="modal">Aceptar</button>
    </div>
  </div>
</div>
@endsection
