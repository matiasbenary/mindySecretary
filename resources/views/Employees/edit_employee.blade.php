@extends('layouts.app')
@section('content')
    <div id="msg" class="col-xs-12 alert" style="display:none" ></div>
    <h3>Datos del empleado</h3>
    <form action="/employees_info/{{$entity->id}}" method="POST" onsubmit="saveData(event,this);">
        {{ method_field('PUT') }}
        @include('Employees.employeeForm')
       
    </form>
   @foreach ($errors as $error)
   <h4>{{$error}}</h4>
	@endforeach
@endsection
