<div class="modal fade" id="employeeDetailModal" style="z-index:99999;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Información del usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-md-12">
            <div class="col-md-6">
              <label for="dni" class="col-2 col-form-label">DNI</label>
              <span name="dni" class="" id="dni">213</span>
            </div>

             
            <div class="col-md-6">
                <label for="nombre" class="col-2 col-form-label">Nombre</label>
                <span name="nombre"  type="text" id="nombre"> </span>
            </div>

      </div>

      <div class=" col-md-12">
          <div class="col-md-6">
            <label for="apellido" class="col-2 col-form-label">Apellido</label>
            <span name="apellido"  type="text" id="apellido"> </span>
        </div>
          <div class="col-md-6">
            <label for="fecha_nacimiento" class="col-2 col-form-label">Fecha nacimiento</label>
            <span name="fecha_nacimiento"  type="text" id="fecha_nacimiento"> </span>
        </div>

        </div>

        <div class=" col-md-12">
          <div class="col-md-6">
            <label for="fecha_ingreso_empresa" class="col-2 col-form-label">Fecha Ingreso</label>
            <span name="fecha_ingreso_empresa"  type="text" id="fecha_ingreso_empresa"> </span>
        </div>

          <div class="col-md-6">
            <label for="email" class="col-2 col-form-label">Email</label>
            <span name="email"  type="text" id="email"> </span>
        </div>
        </div>


        <div class=" col-md-12">
          <div class="col-md-6">
            <label for="lugar_nacimiento" class="col-2 col-form-label">Lugar de nacimiento</label>
            <span name="lugar_nacimiento"  type="text" id="lugar_nacimiento"> </span>
        </div>

          <div class="col-md-6">
            <label for="telefono" class="col-2 col-form-label">Teléfono</label>
            <span name="telefono"  type="text" id="telefono"> </span>
        </div>
        </div>


        <div class=" col-md-12">
          <div class="col-md-6">
            <label for="sexo" class="col-2 col-form-label">Sexo</label>
            <span name="sexo"  type="text" id="sexo"> </span>
        </div>
          <div class="col-md-6">
            <label for="estado_civil" class="col-2 col-form-label">Estado civil</label>
            <span name="estado_civil"  type="text" id="estado_civil"> </span>
        </div>
        </div>

        <div class=" col-md-12">
          <div class="col-md-6">
            <label for="direccion" class="col-2 col-form-label">Dirección</label>
            <span name="direccion"  type="text" id="direccion"> </span>
        </div>
          <div class="col-md-6">
            <label for="ciudad" class="col-2 col-form-label">Ciudad</label>
            <span name="ciudad"  type="text" id="ciudad"> </span>
        </div>
        </div>

        <div class=" col-md-12">
          <div class="col-md-6">
            <label for="provincia" class="col-2 col-form-label">Provincia</label>
            <span name="provincia"  type="text" id="provincia"> </span>
        </div>

          <div class="col-md-6">
            <label for="codigo_postal" class="col-2 col-form-label">Codigo postal</label>
            <span name="codigo_postal"  type="text" id="codigo_postal"> </span>
        </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
$('#employeeDetailModal').on('shown', function () {
    getUserData();
});

 function getUserData(){
        $.ajax({url:'/employees_info/{{Auth::user()->id}}/detail',method:'GET'}).done(function(response){
            $.each(response,function(key,value){
                $("span[id="+key+"]").text(value);
            });
        });

    }
</script>
