@extends('layouts.app')

@section('content')
    <h3>Historial de solicitudes</h3>
    <table id="events_1" class="col-md-offset-2 display">
    </table>
    <form name="employeedata" action="/employees_info" method="GET">
        {{ csrf_field() }}
    </form>
<script>
    $(document).ready(function(){
        var events = [];
        var url = "/event_all";
        $.ajax({url:url,data:{},method:'GET',async:false}).done(function(response){
            events = response;
        });
        //var dataset = [[1231,'jonathan','baez','adasd','','',''],[1231,'jonathan','baez','adasd','','','']];
        var table = $('#events_1').DataTable(
            {
                data:events,
                columnDefs: [
                    { }
                ],
                columns: [
                    { title: "Fecha Pedido",data:"created_at" },
                    { title: "Tipo de Solicitud",data:"benefit.nombre" },
                    { title: "Estado",data:"estado" },
                    { title: "Fecha Inicio",data:"start"}

                ],
                oLanguage: {
                "sSearch": "Buscar solicitud: ",
                "oPaginate": {
                          "sPrevious": "Anterior",
                          "sNext": "Siguiente",
                        },
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ hasta _END_ de _TOTAL_",
                "sInfoEmpty": "Mostrando 0 hasta 0 de 0 resultados"
                },                
                bLengthChange:false
            }
        );
    });
</script>
@endsection