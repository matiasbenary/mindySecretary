@extends('layouts.app')

@section('content')
    <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>

    <script>

        /*
         * DONUT CHART
         * -----------
         */
        $(document).ready(function() {
            /*$.ajax({url:'get_sectors_info',async: false,success:function(resultado){
                $mySectorsList = resultado;
            }});*/
            $mySectorsList = [{!!$data !!}];
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Cantidad de empleados por sector'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data:$mySectorsList
                }]
            });
            console.log($mySectorsList);
        });
    </script>
@endsection