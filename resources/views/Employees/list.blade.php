@extends('layouts.app')

@section('content')
    <h3>Listado de empleados</h3>
    <table id="employeesList" class="col-md-offset-2 display">

    </table>

    <div class="modal fade" id="modal" style="z-index:99999 ;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 style="display:inline" class="modal-title" id="exampleModalLongTitle">Baja de empleado</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>¿Confirma la baja del empleado seleccionado?</h5>
      </div>
      <div class="modal-footer"><button class="btn btn-default" data-dismiss="modal">Cancelar</button><button class="btn btn-default" onclick="eraseEmployee()">Confirmar</button></div>
    </div>
  </div>
</div>

    <form name="employeedata" action="/employees_info" method="GET">
        {{ csrf_field() }}
    </form>
<script>
    $(document).ready(function(){        
        var dataset ={!! $data !!};
        console.log(dataset);
        //var dataset = [[1231,'jonathan','baez','adasd','','',''],[1231,'jonathan','baez','adasd','','','']];
        var table = $('#employeesList').DataTable(
            {
                data:dataset,
                columnDefs: [
                    {
                }
                ],
                columns: [
                    { title: "DNI",data:"dni" },
                    { title: "Nombre",data:"nombre" },
                    { title: "Apellido",data:"apellido"  },
                    { title: "Sector",data:"id_sector"  },
                    {"render": function ( data, type, full, meta ,row) {                    
                    return '<button style="z-index:999" onclick="'+"$('#modal').trigger('openmodal','"+full.id+"')"+'"class="eraseEmployee btn btn-primary" id="'+full.id+'">Baja de empleado</button>';
                    }}
                ],
                oLanguage: {
                "sSearch": "Buscar empleado: "
                },
                "oLanguage": {
                    "sSearch": "Buscar empleado: ",
                    "oPaginate": {
                          "sPrevious": "Anterior",
                          "sNext": "Siguiente",
                        },
                    "sZeroRecords": "No se encontraron resultados",
                  "sInfo": "Mostrando _START_ hasta _END_ de _TOTAL_",
                  "sInfoEmpty": "Mostrando 0 hasta 0 de 0 resultados"
                  },
                bLengthChange:false,
            }
        );
        //editar empleado
        $('tbody').on('click','td',  function () {
            var hasButton = $(this).find("button").length >0;
            if(!hasButton){
                var data = table.row( this ).data();
                $form = $("form[name=employeedata]");
                var input = "<input name='id' value='"+data.dni+"' type='hidden'></input>";
                var newAction = $form.attr("action") + "/"+data.id + "/edit";
                $form.attr("action",newAction);
                $form.submit();
            }else{
                return false;
                }
            }
        );
    });
    $("#modal").on("openmodal",function(event,employeeId){
        debugger;
        $("#modal").data('employeeId',employeeId);
        $("#modal").modal();
    });
    function eraseEmployee (){
        var employeeId = $('#modal').data('employeeId')
        $.ajax({url:"/employees_info/"+employeeId,
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method:"DELETE"}).done(function(response){console.log(response);});
        window.location.replace("/employees_info");
        };
</script>
@endsection