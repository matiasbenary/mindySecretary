@section('content')

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Solicitud</h3>
	</div>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table no-margin">
				<thead>
					<tr>
						<th>ID Solicitud</th>
						<th>Tipo de Solicitud</th>
						<th>Estado</th>
						<th>Desde</th>
						<th>Hasta</th>
						<th>Solicitante</th>
						<th>Solicitado</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2</td>
						<td>Cambio de Franco</td>
						<td> <span class="label label-success">Autorizado</span></td>
						<td>14/10/2017</td>
						<td>17/10/2017</td>
						<td>Nicolás Benquerenca</td>
						<td>Pepe</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="box-footer clearfix">
	<a href="#" class="btn btn-sm btn-info btn-flat pull-left">Aceptar</a>
	<a href="#" class="btn btn-sm btn-info btn-flat pull-left label-danger">Rechazar</a>
	</div>
</div>


@endsection