@extends('layouts.app')
@section('content')
    <div id="msg" class="col-xs-12 alert" style="display:none" ></div>


    <h3>Nuevo sector</h3>

    <form action="/sectors" method="POST" onsubmit="saveData(event,this);">
        @include('Sectors.sectorForm')

    </form>
	@foreach ($errors as $error)
   <h4>{{$error}}</h4>
	@endforeach

@endsection
