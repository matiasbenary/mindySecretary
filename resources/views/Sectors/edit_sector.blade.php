@extends('layouts.app')
@section('content')
    <div id="msg" class="col-xs-12 alert" style="display:none" ></div>
    <h3>Datos del sector</h3>
    <form action="/sectors/{{$entity->id}}" method="POST" onsubmit="saveData(event,this);">
        {{ method_field('PUT') }}
        @include('Sectors.sectorForm')
       
    </form>
   @foreach ($errors as $error)
   <h4>{{$error}}</h4>
	@endforeach
@endsection
