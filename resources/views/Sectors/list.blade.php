@extends('layouts.app')

@section('content')
    <h3>Todos los Sectores</h3>
    <table id="sector_1" class="col-md-offset-2 display">
    </table>


   <div class="modal fade" id="modal" style="z-index:99999 ;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 style="display:inline" class="modal-title" id="exampleModalLongTitle">Baja de sector</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>¿Confirma la baja del sector seleccionado?</h5>
      </div>
      <div class="modal-footer"><button class="btn btn-default" data-dismiss="modal">Cancelar</button><button class="btn btn-default" onclick="eraseEmployee()">Confirmar</button></div>
    </div>
  </div>
</div>

    <form name="sectordata" action="/sectors" method="GET">
        {{ csrf_field() }}
    </form>
<script>
    $(document).ready(function(){
        var sectors = {!!$sectors!!};
        var url = "/sectors";
        //var dataset = [[1231,'jonathan','baez','adasd','','',''],[1231,'jonathan','baez','adasd','','','']];
        var table = $('#sector_1').DataTable(
            {
                data:sectors,
                columnDefs: [
                    { }
                ],
                columns: [
                    { title: "Nombre sector",data:"descripcion" },
                    { title: "Cant. Empleados",data:"cantidad" },
                    { title: "Persona a cargo",data:"acargo" },
                     {"render": function ( data, type, full, meta ,row) {                    
                    return '<button style="z-index:999" onclick="'+"$('#modal').trigger('openmodal','"+full.id+"')"+'"class="eraseEmployee btn btn-primary" id="'+full.id+'">Baja de sector</button>';
                    }}
                ],
                oLanguage: {
                "sSearch": "Buscar sector: ",
                "oPaginate": {
                          "sPrevious": "Anterior",
                          "sNext": "Siguiente",
                        },
                "sZeroRecords": "No se encontraron resultados",
                "sInfo": "Mostrando _START_ hasta _END_ de _TOTAL_",
                "sInfoEmpty": "Mostrando 0 hasta 0 de 0 resultados"
                },                
                bLengthChange:false
            }
        );

         $('tbody').on('click','td',  function () {
            var hasButton = $(this).find("button").length >0;
            if(!hasButton){
                var data = table.row( this ).data();
                $form = $("form[name=sectordata]");
                var input = "<input name='id' value='"+data.id+"' type='hidden'></input>";
                var newAction = $form.attr("action") + "/"+data.id + "/edit";
                $form.attr("action",newAction);
                $form.submit();
            }else{
                return false;
                }
            }
        );


 $("#modal").on("openmodal",function(event,employeeId){        
        $("#modal").data('employeeId',employeeId);
        $("#modal").modal();
    });
    });
     function eraseEmployee (){
        var employeeId = $('#modal').data('employeeId')
        $.ajax({url:"/sectors/"+employeeId,
         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method:"DELETE"}).done(function(response){});
        window.location.replace("/sectors");
        };

</script>
@endsection
