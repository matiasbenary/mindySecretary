{{ csrf_field() }}
    <div class=" col-md-12"> 
        <div class=" col-md-6">
            <label for="nombre-input" class="col-2 col-form-label">Nombre</label>
            <div class="col-10">
                <input name="descripcion" class="form-control" type="text" value="{{$entity->descripcion}}" id="nombre-input">
            </div>
            <label for="nombre-input" class="col-2 col-form-label">Persona a cargo (DNI)</label>
            <div class="col-10">
                <input name="acargo" class="form-control" type="text" value="{{$entity->incharge()->dni}}" id="acargo-input">
            </div>
            <label for="dni-input" lass="col-2 col-form-label">{{$errors->first('descripcion')}}</label>
        </div>

        <!--<div class=" col-md-6">
            <label for="example-search-input" class="col-2 col-form-label">Responsable a cargo</label>
            <div class="col-10">
                <input  name="nombre" class="form-control" type="text" value="{{$entity->nombre}}" id="name-input" >
            </div>
            <label for="name-input" lass="col-2 col-form-label">{{$errors->first('name')}}</label>
        </div>-->
    </div>
      <div class=" col-md-12"> 
        <div class="col-md-6">
            <button type="submit" class="btn btn-default" >Guardar</button>
        </div>
        <div class="col-md-6">
            <a href="{{ URL::previous()}}"> <button id="backButtonForm" type="button" class="btn btn-default" >Volver
            </button></a>
        </div
    </div>
    

<script>
    function saveData(event,form){
        event.preventDefault();
        var data =new FormData();
        data = $(form).serializeArray();
        $.ajax({url:form.action,data:data,method:form.method}).done(function(response){
            $("#msg").removeClass("alert-danger");
            $("#msg").addClass("alert-success");
            $("#msg").html("Guardado exitosamente");
            $("#usuarioCreado").find("p[name=usuario]").html(response.usuario);
            $("#usuarioCreado").find("p[name=password]").html(response.password);
            $("#usuarioCreado").modal();
        }).fail(function(){
            $("#msg").removeClass("alert-success");
            $("#msg").addClass("alert-danger");
            $("#msg").text("No se pudieron guardar los cambios");
        }).always(function(){
            $("#msg").fadeIn();
            setTimeout(function(){$("#msg").fadeOut()},2000);
        });
}
    

</script>