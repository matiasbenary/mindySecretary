@extends('layouts.app')
@section('content')
    @foreach($errors->all() as $error){
    <p>{{$error}}</p>
    }
    @endforeach
    <div id="err">
    </div>

    <form action="" method="POST" onsubmit="saveDataWithAJAX(event,this);">
        {{ method_field('PUT') }}
        @include('layouts.formTemplate')
    </form>
    <script>
        var messages = {200:['Transacción completa!','alert alert-success'],
            422:['Transacción fallida','alert alert-danger']
        }
        function saveDataWithAJAX(event,form){
            event.preventDefault();
            var csrfToken = document.getElementsByName('csrf-token')[0].content;
            var xhttp = new XMLHttpRequest();
            var data =new FormData();
            $(form).serializeArray().map(function (elem){
                return data.append(elem.name, elem.value);
            });
            debugger;
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && messages[this.status]!=undefined){
                    document.getElementById("err").innerHTML = "<div class='"+messages[this.status][1]+"'><strong>"+messages[this.status][0]+"</strong></div>";
                }
            };
            xhttp.open("POST", form.action.replace('/edit',''), true);
            xhttp.setRequestHeader('Accept','text/json')
            xhttp.setRequestHeader("X-CSRF-TOKEN",csrfToken);
            xhttp.send(data);
        }
    </script>
@endsection
