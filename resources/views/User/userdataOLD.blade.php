@extends('layouts.app')
@section('content')
  @foreach($errors->all() as $error){
    <p>{{$error}}</p>
  }
  @endforeach
  <form action="" method="POST">
        @include('layouts.formTemplate')
  </form>
@endsection
