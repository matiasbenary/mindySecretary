<!DOCTYPE html
<html lang="{{ config('app.locale') }}">
<html style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="{{ asset('js/jquery.cookie.js') }}"></script>

</head>
<body style="display: flex; height: 100%; flex-direction: column">
<div>
        <nav class="navbar navbar-default navbar-fixed-top">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                     </ul>
                </div>
        </nav>
    </div>
    <!-- No need for 'flex-direction: row' because it's the default value -->
    <div style="display: flex; flex: 1">
      <div style="flex: 1; overflow: auto">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Panel: Inicio Sesión</div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" autocomplete="off" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Contraseña</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Iniciar Sesión
                                        </button>

                                        <a class="btn btn-primary" href="{{ route('password.request') }}">
                                            Cambiar Contraseña
                                        </a>
                                    </div>
                                </div>

                                <h2><span  class="line-center">O inicia sesión con tu red social</span></h2>
                                <div class="btn btn-group-justified">
                                    <a href="/login1" class="btn-social btn-primary btn-social-icon btn-twitter btn-social-twitter"><span class="fa fa-twitter fa-2x"></span></a>
                                    <a href="/login2" class="btn-social btn-primary btn-social-icon btn-facebook btn-social-facebook"><span class="fa fa-facebook fa-2x"></span></a>
                                    <a href="/login3" class="btn-social btn-primary btn-social-icon btn-google btn-social-google"><span class="fa fa-google fa-2x"></span></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#password').tooltip({title: "Si aún no la obtuviste, pasa por la oficina de RRHH", placement: "right"});
    $.removeCookie('pop');
});
</script>

</body>
</html>
