@extends('layouts.app')
@section('content')

<div class="container">
	<div class="col-md-8" style="width: 750px;">
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-left: -15px;">
	    	<!-- Indicators -->
	    	<ol class="carousel-indicators">
	      		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	      		<li data-target="#myCarousel" data-slide-to="1"></li>
	      		<li data-target="#myCarousel" data-slide-to="2"></li>
	    	</ol>

	    	<!-- Wrapper for slides -->
	    	<div class="carousel-inner">
	      		<div class="item active">
	        		<img src="img/home_5.jpg" alt="Los Angeles" style="width:100%;">
	      		</div>

	      		<div class="item">
	        		<img src="img/home_5.jpg" alt="Chicago" style="width:100%;">
	      		</div>
	    
	      		<div class="item">
	        		<img src="img/home_5.jpg" alt="New york" style="width:100%;">
	      		</div>
	    	</div>

	    	<!-- Left and right controls -->
	    	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
	      		<span class="glyphicon glyphicon-chevron-left"></span>
	      		<span class="sr-only">Previous</span>
	    	</a>
	    	<a class="right carousel-control" href="#myCarousel" data-slide="next">
	      		<span class="glyphicon glyphicon-chevron-right"></span>
	      		<span class="sr-only">Next</span>
	    	</a>
	    </div>
	</div>
    <div class="col-md-3" style="margin-top: 20px;">
		<div class="box box-info" style="border-top-color: violet;background-color: pink;height: 206px; width: 362px;">
			<div class="box-header" style="background-color: pink;">
				<h3 class="box-title">¡HOY CUMPLEN AÑOS!</h3>
			</div>
			<div class="box-body" style="height: 120px; overflow: auto;">
				<div class="table-responsive">
					<table id="birthdaysTodayList" class="table no-margin">
						<tbody >
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="box-footer" style="background-color: transparent;">
				<tr>
					<td>
						<a class = "btn btn-primary " style="background-color: #bc3ca0; border-color: black; margin-left: 70px;" href="/birthdays">Ver Cumpleaños del mes</a>
					</td>
				</tr>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="col-md-11 ">
		<!-- Contextual button for informational alert messages -->
		<button style="width: 100%; margin: 20px 40px; line-height: 34px; position: relative;user-select:none;outline: none !important; cursor: default;" type="button" class="btn btn-success ribbon"><h4>Beneficios Disponibles</h4></button>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-11" style="margin-left: 80px;">
			@foreach($empleado->benefits as $benefit)
				<div style="margin-right: 25px;" class="col-md-3">
					<div class="info-box">

					<span class="info-box-icon bg-aqua">
						<i class="fa fa-users fa-lg fa-benefits"></i>
					</span>
						<div class="info-box-content">
							<span class="info-box-text">{{$benefit->nombre}}</span>
							<span class="info-box-number">{{$benefit->pivot->cantidad}}</span>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
<div class="container">
	<div class="col-md-11 ">
		<!-- Contextual button for informational alert messages -->
		<button style="width: 100%; margin: 20px 40px; line-height: 34px; position: relative;user-select:none;outline: none !important; cursor: default;" type="button" class="btn btn-success ribbon"><h4>Solicitudes</h4></button>
	</div>
</div>
<div class="col-md-6 " style="    width: 48%;">
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">Mis Solicitudes</h3>
			<button data-toggle="modal" data-target="#createRequestModal">Solicitar</button>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table id="myRequests" class="table no-margin display">
					<thead>
							<tr>
								<th>Tipo de Solicitud</th>
								<th>Estado</th>
								<th>Certificado</th>
								<th>Día de comienzo</th>
								<th>Día de finalizacion</th>
							</tr>
					</thead>
					<tbody>
					@foreach($empleado->event as $event)
						<tr>
							<th>{{$event->benefit['nombre']}}</th>
							<th>{{$event->estado}}</th>
							<th>
							@if($event->benefit['need_certificate'])
								@if($event->filepath)
									Presentado
								@else
									Falta Presentar
								@endif
                            @else
								No Requiere
							@endif
							</th>
							<th>{{$event->start}}</th>
							<th>{{$event->finish}}</th>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<tr>
				<td>
					<a class = "btn btn-warning " style="border-color: black; margin-left: 140px;" href="/events_list">Ver historial de solicitudes</a>
				</td>
			</tr>
		</div>
	</div>
</div>




<div class="modal fade" id="createRequestModal" role="dialog" >
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Nueva solicitud</h4>
			</div>
			<div class="modal-body">
				<form class="row" style="padding:10px" name="userRequestForm" action="/myrequest" method="POST">
					{{csrf_field()}}
					<div class="col-xs-12">
						<div class="form-group">
							<label for="tipoSolicitud">Tipo de solicitud:</label>
							<select class="form-control" name="benefit_id">
								@foreach($empleado->benefits as $benefit)
									<option value="{{$benefit->id}}">{{$benefit->nombre}}</option>
								@endforeach
							</select>
						</div>
						
						<div class="col-xs-12 form-group">
		                    <div class="col-xs-6">
		                        <label>Desde</label>
		                        <input class="form-control" type="date" name="start_day">
		                    </div>

							<div class="col-xs-6 ">
		                        <label>Hasta</label>
		                        <input class="form-control" type="date" name="finish_day">
		                    </div>
	                    </div>

	                    <div class="col-xs-12 form-group">
	                        <label>Certificado</label>
	                        <input class="form-control" type="text" name="filepath">
	                    </div>
						<input type="hidden" name="employee_id" value="{{Auth::user()->employee->id}}">
						<input type="hidden" name="status" value="ingresado">
						</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" onclick="sendUserRequest(event)" class="btn btn-default">Enviar</button>
			</div>
		</div>

	</div>
</div>

<div class="modal fade" id="createRequestModal1" role="dialog" >
	<div class="modal-dialog" style="width: 900px;">

		<!-- Modal content-->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Solicitud</h3>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>ID Solicitud</th>
								<th>Tipo de Solicitud</th>
								<th>Estado</th>
								<th>Desde</th>
								<th>Hasta</th>
								<th>Solicitante</th>
								<th>Solicitado</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>2</td>
								<td>Cambio de Franco</td>
								<td> <span class="label label-success">Autorizado</span></td>
								<td>14/10/2017</td>
								<td>17/10/2017</td>
								<td>Nicolás Benquerenca</td>
								<td>Pepe</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box-footer clearfix" style="display: grid;">
			<a href="#" class="btn btn-sm btn-info btn-flat">Aceptar</a>
			<a href="#" class="btn btn-sm btn-info btn-flat label-danger">Rechazar</a>
			</div>
</div>

	</div>
</div>
<div class="modal fade in" id="formSaludo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block;">
    <div class="modal-dialog">
         <div class="modal-content">
              <div class="modal-header">
                  <h5 style="text-decoration: underline;text-transform: uppercase;"><strong>Bienvenido al sistema de Gestion de RRHH</strong></h5>
                  
              </div> 
              <div class="modal-body" id="modal-body11">
              	<h5><strong>Le recomendamos ver el siguiente video, donde se le explica el funcionamiento de la aplicacion.</strong></h5>
              	<iframe style="border-width: 5px;border-style: solid; border-radius: 4px; border-color: black;" id="cartoonVideo" width="560" height="315" src="//www.youtube.com/embed/TYKcDULZsMk" frameborder="0" allowfullscreen></iframe>
               </div> 
                <div class="modal-footer">

                      <a class="btn btn-link" data-dismiss="modal" style="font-size:15px;color:blue;cursor: pointer;text-decoration: none;"><strong>Continuar navegando...</strong></a>
              </div>  
        </div>
    </div>
</div>


<script>


$(document).ready(function(){
	 var employeess = [];
        var url = "birthdaysTodayListHome";
        $.ajax({url:url,data:{},method:'GET',async:false}).done(function(response){
            employeess = response.employeess;
            for(i=0;i<employeess.length;i++){
 				$("#birthdaysTodayList tbody").append("<tr><td>"+employeess[i].nombre+" "+employeess[i].apellido+"</td></tr>");
            }
        });
    $(".modal").on("hidden.bs.modal", function() {
    	$("#modal-body11").html("");
  	});

  	if ($.cookie('pop') == null) {
         $('#formSaludo').modal('show');
         $.cookie('pop', 'Sesion_Iniciada', {expires: 1});
     }
     else {
     	$('#formSaludo').hide();
     }
});

function sendUserRequest (event){	
	$form = $("form[name=userRequestForm]");
	$.ajax({url:'/sendUserRequest',method:'post',data:$form.serialize()}).done(function(response){		
		location.reload();
	});
}

</script>



@endsection