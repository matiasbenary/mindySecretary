@extends('layouts.app')
@section('content')

<div class="col-md-4" style="margin-top: 20px;">
  <!-- Info Boxes Style 2 -->
  <div class="info-box bg-yellow" id="franco">
    <span class="info-box-icon"><i class="fa fa-users fa-lg fa-benefits"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Cambios de Franco</span>
      <span class="info-box-number"></span>

      <div class="progress">
        <div class="progress-bar" style="width: 50%"></div>
      </div>
          <span class="progress-description">
            50% Incrementadas en 30 dias
          </span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
  <div class="info-box bg-green" id="dias_estudio">
    <span class="info-box-icon"><i class="fa fa-users fa-lg fa-benefits"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Dias de Estudio</span>
      <span class="info-box-number"></span>

      <div class="progress">
        <div class="progress-bar" style="width: 20%"></div>
      </div>
          <span class="progress-description">
            20% Incrementadas en 30 dias
          </span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
  <div class="info-box bg-red" id="cambio_horario">
    <span class="info-box-icon"><i class="fa fa-users fa-lg fa-benefits"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Cambios de Horario</span>
      <span class="info-box-number">114,381</span>

      <div class="progress">
        <div class="progress-bar" style="width: 70%"></div>
      </div>
          <span class="progress-description">
            70% Incrementadas en 30 dias
          </span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
  <div class="info-box bg-aqua" id="vacaciones">
    <span class="info-box-icon"><i class="fa fa-users fa-lg fa-benefits"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Vacaciones</span>
      <span class="info-box-number"></span>

      <div class="progress">
        <div class="progress-bar" style="width: 40%"></div>
      </div>
          <span class="progress-description">
            40% Incrementadas en 30 dias
          </span>
    </div>
    <!-- /.info-box-content -->
    </div>
</div>
<!-- /.info-box -->

<div class="col-md-7" style="margin-top: 20px;">
  <div class="box" style="margin-left: 2.1%; height: 405px;width: 730px;">
    <div class="box-header">
      <h3 class="box-title">Solicitudes Pendientes</h3>

      <div class="box-tools">
        <ul class="pagination pagination-sm no-margin pull-right">
          <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html#">«</a></li>
          <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html#">1</a></li>
          <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html#">2</a></li>
          <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html#">3</a></li>
          <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html#">»</a></li>
        </ul>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table">
        <tbody><tr>
          <th style="width: 10px">#</th>
          <th style="width: 40%;">Solicitud</th>
          <th style="width: 40%;">Progress</th>
          <th style="width: 40px">Label</th>
          <th style="width: 40px">Abrir Solicitud</th>
        </tr>
        <tr>
          <td>1.</td>
          <td>Cambio de Franco</td>
          <td>
            <div class="progress progress-xs">
              <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
            </div>
          </td>
          <td><span class="badge bg-red">55%</span></td>
        </tr>
        <tr>
          <td>2.</td>
          <td>Cambio de Franco</td>
          <td>
            <div class="progress progress-xs">
              <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
            </div>
          </td>
          <td><span class="badge bg-yellow">70%</span></td>
        </tr>
        <tr>
          <td>3.</td>
          <td>Vacaciones</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
            </div>
          </td>
          <td><span class="badge bg-light-blue">30%</span></td>
        </tr>
        <tr>
          <td>4.</td>
          <td>Día de Examen</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-success" style="width: 90%"></div>
            </div>
          </td>
          <td><span class="badge bg-green">90%</span></td>
        </tr>
        <tr>
          <td>5.</td>
          <td>Cambio de Franco</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-success" style="width: 90%"></div>
            </div>
          </td>
          <td><span class="badge bg-green">90%</span></td>
        </tr>
        <tr>
          <td>6.</td>
          <td>Cambio de Franco</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-success" style="width: 90%"></div>
            </div>
          </td>
          <td><span class="badge bg-green">90%</span></td>
        </tr>
        <tr>
          <td>7.</td>
          <td>Cambio de Franco</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-success" style="width: 90%"></div>
            </div>
          </td>
          <td><span class="badge bg-green">90%</span></td>
        </tr>
        <tr>
          <td>8.</td>
          <td>Cambio de Franco</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-success" style="width: 90%"></div>
            </div>
          </td>
          <td><span class="badge bg-green">90%</span></td>
        </tr>
      </tbody></table>
    </div>
    <!-- /.box-body -->
  </div>
</div>

<!-- DONUT CHART -->

<div class="col-md-6">
	<div class="box box-danger">
	<div class="box-header with-border">
	  <h3 class="box-title">Informe Solicitudes</h3>

	  <div class="box-tools pull-right">
	    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	    </button>
	    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	  </div>
	</div>
	<!--<div class="box-body chart-responsive">
	  <div class="chart" id="sales-chart" style="height: 300px; position: relative;"><svg height="300" version="1.1" width="510" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#3c8dbc" d="M254.75,243.33333333333331A93.33333333333333,93.33333333333333,0,0,0,342.9777551949771,180.44625304313007" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#3c8dbc" stroke="#ffffff" d="M254.75,246.33333333333331A96.33333333333333,96.33333333333333,0,0,0,345.81364732624417,181.4248826052307L382.3651459070204,194.03833029452744A135,135,0,0,1,254.75,285Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#f56954" d="M342.9777551949771,180.44625304313007A93.33333333333333,93.33333333333333,0,0,0,171.03484627831412,108.73398312817662" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#f56954" stroke="#ffffff" d="M345.81364732624417,181.4248826052307A96.33333333333333,96.33333333333333,0,0,0,168.34400205154566,107.40757544301087L129.17726941747117,88.10097469226493A140,140,0,0,1,387.0916327924656,195.6693795646951Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#00a65a" d="M171.03484627831412,108.73398312817662A93.33333333333333,93.33333333333333,0,0,0,254.72067846904883,243.333328727518" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#00a65a" stroke="#ffffff" d="M168.34400205154566,107.40757544301087A96.33333333333333,96.33333333333333,0,0,0,254.71973599126827,246.3333285794739L254.7075884998742,284.9999933380171A135,135,0,0,1,133.6620097954186,90.31165416754118Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="254.75" y="140" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: 800; font-stretch: normal; font-size: 15px; line-height: normal; font-family: Arial;" font-size="15px" font-weight="800" transform="matrix(1.4454,0,0,1.4454,-113.4508,-67.2487)" stroke-width="0.6918712797619048"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="6">Autorizadas</tspan></text><text x="254.75" y="160" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: Arial;" font-size="14px" transform="matrix(1.9444,0,0,1.9444,-240.5972,-143.5556)" stroke-width="0.5142857142857143"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="5">30</tspan></text></svg></div>
	</div>-->
	<!-- /.box-body -->
  <div id="container">
    
  </div>
	</div>
</div>

<!-- BAR CHART -->
<div class="col-md-5">
  <div class="box box-success" style="width: 550px; ">
    <div class="box-header with-border">
      <h3 class="box-title">Resumen Mensual Solicitudes</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body chart-responsive">
      <div class="chart" id="bar-chart" style="height: 300px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="300" version="1.1" width="510" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.5px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="33.5" y="261" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">0</tspan></text><path fill="none" stroke="#aaaaaa" d="M46,261H484.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="33.5" y="202" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">25</tspan></text><path fill="none" stroke="#aaaaaa" d="M46,202H484.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="33.5" y="143" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">50</tspan></text><path fill="none" stroke="#aaaaaa" d="M46,143H484.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="33.5" y="84" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">75</tspan></text><path fill="none" stroke="#aaaaaa" d="M46,84H484.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="33.5" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">100</tspan></text><path fill="none" stroke="#aaaaaa" d="M46,25H484.5" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="453.17857142857144" y="273.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">2012</tspan></text><text x="327.89285714285717" y="273.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">2010</tspan></text><text x="202.60714285714286" y="273.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">2008</tspan></text><text x="77.32142857142857" y="273.5" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" dy="4">2006</tspan></text><rect x="53.83035714285714" y="25" width="21.99107142857143" height="236" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="78.82142857142857" y="48.60000000000002" width="21.99107142857143" height="212.39999999999998" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="116.47321428571428" y="84" width="21.99107142857143" height="177" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="141.46428571428572" y="107.6" width="21.99107142857143" height="153.4" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="179.11607142857142" y="143" width="21.99107142857143" height="118" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="204.10714285714283" y="166.60000000000002" width="21.99107142857143" height="94.39999999999998" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="241.75892857142858" y="84" width="21.99107142857143" height="177" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="266.75" y="107.6" width="21.99107142857143" height="153.4" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="304.4017857142857" y="143" width="21.99107142857143" height="118" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="329.39285714285717" y="166.60000000000002" width="21.99107142857143" height="94.39999999999998" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="367.0446428571429" y="84" width="21.99107142857143" height="177" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="392.03571428571433" y="107.6" width="21.99107142857143" height="153.4" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="429.68750000000006" y="25" width="21.99107142857143" height="236" r="0" rx="0" ry="0" fill="#00a65a" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="454.6785714285715" y="48.60000000000002" width="21.99107142857143" height="212.39999999999998" r="0" rx="0" ry="0" fill="#f56954" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="left: 42.423px; top: 107px; display: none;"><div class="morris-hover-row-label">2006</div><div class="morris-hover-point" style="color: #00a65a">
  CPU:
  100
</div><div class="morris-hover-point" style="color: #f56954">
  DISK:
  90
</div></div></div>
</div>

<script>
    $(document).ready(function(){
        $.ajax({url:'/getCompanyRequestsCount'}).done(function(response){
            var francos = response.filter(function(obj){return obj.nombre == 'franco'})[0];
            var dias_estudio = response.filter(function(obj){return obj.nombre == 'dia de estudio'})[0];
            var vacaciones = response.filter(function(obj){return obj.nombre == 'vacaciones'})[0];
            var cambio_horario = response.filter(function(obj){return obj.nombre == 'cambio horario'})[0];
           
            $('#franco .info-box-number').html(francos.cantidad);
            $('#dias_estudio .info-box-number').html(dias_estudio.cantidad);
            $('#vacaciones .info-box-number').html(vacaciones.cantidad);
            $('#cambio_horario .info-box-number').html(cambio_horario.cantidad);

        });
         $.ajax({url:'/getCompanyRequestStatusCount'}).done(function(response){
           $mySectorsList = response;
           //en hicharts se necesita por cada pedazo del grafico (objeto) que tenga un parametro name y un parametro 'y'
           //nose porquqe.
           var nombres = {Desaprobado:'Desaprobadas',Pendiente:'Pendientes',Aprobado:'Aprobadas'}
           $cantidades = $mySectorsList.map(function(elem){return {name:nombres[elem.estado],y:elem.cantidad};});
           $cantidades = $cantidades.filter(function(obj){return obj.name!=undefined});
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Estado de solicitudes'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Estado de solicitudes',
                    colorByPoint: true,
                    data:$cantidades
                }]
            });
         });

    });
</script>

@endsection