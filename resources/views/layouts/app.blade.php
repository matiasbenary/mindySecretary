<!DOCTYPE html
<html lang="{{ config('app.locale') }}">
<html style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidenav.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/admin.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/datatables/datatables.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script src="{{ asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/bootstrap.js')}}"></script>
    <script src="{{ asset('js/vue.js')}}"></script>
    <script src="{{ asset('js/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('js/highcharts/exporting.js') }}"></script>
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/morris.min.js') }}"></script>
    <script src="{{ asset('js/analytics.js') }}"></script>
    <script src="{{ asset('js/fastclick.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js')}}"></script>
    <script src="{{ asset('js/jquery.cookie.js') }}"></script>
    <script >

    function loader() {
        $(".loader").hide();
    }
    </script>
</head>
<body onload="loader()" style="display: flex; height: 100%; flex-direction: column">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            
            <!-- Branding Image -->
            <a class="navbar-brand">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                
            </ul>

            <!-- Right Side Of Navbar -->
            @section('role_switch')
             <li id="role_switch_to_admin" class="role_switch checkbox" style="opacity: 0;">
                    <input id="toggle-trigger" onchange="pepe()" type="checkbox" checked data-toggle="toggle" data-on="Usuario" data-off="Administrador" data-onstyle="success" data-offstyle="danger" style="display: none;">
                    <div id="console-event"></div>
                </li>
                <li id="role_switch_to_user" class="role_switch checkbox" style="opacity: 0;margin-top: 10px;">
                    <input id="toggle-trigger" onchange="pepe()" type="checkbox" checked data-toggle="toggle" data-on="Administrador" data-off="Usuario" data-onstyle="danger" data-offstyle="success" style="display: none;">
                    <div id="console-event"></div>
                </li>  
            @endsection
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::user()->isRole('admin'))
                    @yield('role_switch')
                @endif
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle ="dropdown" aria-expandedad = "false">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning notif-amount"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="header "> Tu tienes <b class="notif-amount"></b> notificaciones</li>
                        <li>
                            <div class="slimScrollDiv" style="position: relative;overflow: hidden;width: auto;height: 200px;">
                                <ul id="notifications-list" class="menu" style="overflow: hidden;width: 100%;height: 200px;">
                                </ul>
                            </div>

                        </li>
                        <li class="footer">
                            <a href="#">Ver todas</a>
                        </li>

                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle = "dropdown" area-expanded = "false">
                        <img src="{{ asset('img/image_user_1.png')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->name }}
                            <span class="caret"></span>

                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ asset('img/image_user_1.png')}}" class="img-circle" alt="User Image">
                            <p>{{ Auth::user()->name }}
                            <small>{{ isSet($sector)?$sector->descripcion:'' }}</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            
                            <div class="pull-left">
                                <a href="#" onclick="getUserData()" data-target="#employeeDetailModal" data-toggle="modal" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                    Cerrar Sesión
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
      @include('Employees.employeeDetail')
    <!-- No need for 'flex-direction: row' because it's the default value -->
    <div style="display: flex; flex: 1">
        @if (Auth::check())
        @include('layouts.sidenav')
        @endif
      <div id="root-container" style="flex: 1; overflow: auto">
        <div class="loader"></div>
        @yield('content')
      </div>
    </div>
    <form name="role_switch_form" action="/home" method="POST">
        {{ csrf_field() }}
    </form>
<script>
    $(document).ready(function(){
            getNotifications();
            $("#root-container").css("margin-left",$("#menu, #menu_admin").width()+20);
            $(".role_switch").css("opacity","1");
            var usertype = '{!!isset($usertype)?$usertype:null!!}';
            if(usertype==''){usertype = localStorage.getItem('usertype');}
            localStorage.setItem("usertype",usertype);
            if(usertype == 'admin'){
                $("#role_switch_to_admin").hide();
                $("#menu").hide();
             }
            if(usertype =='user'){
                $("#role_switch_to_user").hide();
                $("#menu_admin").hide();
             }
            
    });

    function getNotifications(){
        $.ajax('getNotifications').done(function(response){
            $.each(response.eventos,function(key,value){
                if(value){
                    if(value.estado=="Desaprobado"){
                        notificacionDesa = "<li><a style='color:red'>Te desaprobaron "+value.cantidad +" "+ value.nombre +"</a></li>";
                        $('#notifications-list').append(notificacionDesa);
                    }
                    else{
                        notificacionApro = "<li><a>Te aprobaron "+value.cantidad +" "+ value.nombre +"</a></li>";
                        $('#notifications-list').append(notificacionApro);
                    }

               $(".notif-amount").html( response.eventos.length) ;
                }

            });
        });
    }

    function pepe() {
        var usertype = localStorage.getItem("usertype");      
        if (usertype == "admin") {
            localStorage.setItem("usertype","user");
            $("#menu").show();
            $("#menu_admin").hide();
        }
        else {
            localStorage.setItem("usertype","admin");
            $("#menu_admin").show();
            $("#menu").hide();
        }
         /*var url = "/home";   
             $.ajax({url:url,data:{usertype:localStorage.getItem("usertype")},method:'GET',async:false}).done(function(response){
        }); */
            $form = $("form[name=role_switch_form]");
            var input = "<input name='usertype' value='"+localStorage.getItem("usertype")+"' type='hidden'></input>";
            $form.append(input);
            $form.submit();
    }

</script>

</body>
</html>