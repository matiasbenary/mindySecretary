<!DOCTYPE html
<html lang="{{ config('app.locale') }}">
<html style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidenav.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/admin.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/datatables/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/datatables/datatables.css')}}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="{{ asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/bootstrap.js')}}"></script>
    <script src="{{ asset('js/vue.js')}}"></script>
    <script src="{{ asset('js/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('js/highcharts/exporting.js') }}"></script>
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script >
    function loader() {
        $(".loader").hide();
    }
    </script>

</head>
<body onload="loader()" style="display: flex; height: 100%; flex-direction: column">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          <!--  <i id="hamburger" onclick="collapseSideMenu()" class="fa fa-bars fa-2x" style="float:left;float: left;margin-top: 12px;margin-left: 10px;" aria-hidden="true"></i>-->
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle ="dropdown" aria-expandedad = "false">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="header"> Tu tienes 10 notificaciones</li>
                        <li>
                            <div class="slimScrollDiv" style="position: relative;overflow: hidden;width: auto;height: 200px;">
                                <ul class="menu" style="overflow: hidden;width: 100%;height: 200px;">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i>
                                            2 cambios de franco autorizados
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </li>
                        <li class="footer">
                            <a href="#">Ver todas</a>
                        </li>

                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle = "dropdown" area-expanded = "false">
                        <img src="{{ asset('img/image_user_1.png')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->name }}
                            <span class="caret"></span>

                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="{{ asset('img/image_user_1.png')}}" class="img-circle" alt="User Image">
                            <p>{{ Auth::user()->name }} 
                            <small>Soporte Técnico</small>
                            <small>Miembro desde Noviembre 2012</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            
                            <div class="pull-left">
                                <a href="#" onclick="getUserData()" data-target="#employeeDetailModal" data-toggle="modal" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                    Cerrar Sesión
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
      @include('Employees.employeeDetail')
    <!-- No need for 'flex-direction: row' because it's the default value -->
    <div style="display: flex; flex: 1">
        @if (Auth::check())
        @include('layouts.sidenav')
        @endif
      <div id="root-container" style="flex: 1; overflow: auto">
        <div class="loader"></div>
        @yield('content')
      </div>
    </div>

<script>
    $(document).ready(function(){
            $("#root-container").css("margin-left",$("#menu").width()+20);
    });

    var width = $("#menu").css("width");
    function collapseSideMenu(){
        var currentWidth = $("#menu").css("width");
        if(currentWidth == '0px') {
            $("#menu").animate({"width": width});
        }else{
            $("#menu").animate({"width": "0px"});
        }
    }

</script>
</body>
</html>