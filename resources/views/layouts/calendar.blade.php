<!DOCTYPE html>
<html>
<head>
<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="css/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="css/scheduler.css" rel="stylesheet">
<script src="plugins/fullcalendar/lib/moment.min.js"></script>
<script src="plugins/fullcalendar/lib/jquery.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.js"></script>
<script src="css/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/fullcalendar/locale-all.js"></script>
<script src="js/scheduler.js"></script>

<script type="text/javascript">    

    function createCalendar(tipoRota){
        var BASEURL = "{{ url('/')}}";
            var calendar = $("#calendar").fullCalendar({
                schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
                locale: 'es',
                theme: true,
                timezone: 'America/Argentina',
                businessHours: true,
                businessHours: tipoRota.businessHours,//aca hay que pasar el parámetro del recurso que queremos que levante el calendario,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                footer : {
                    center: 'custom4 custom1 custom2 custom5 custom6 ',
                    right: 'custom3',
                },
                eventClick: function (calEvent) {
                    $('.tooltiptopicevent').remove();
                    var endDate1 = calEvent.end
                    $('#EventModal #eventid').html(calEvent.id);
                    $('#EventModal #event_title').html(calEvent.title);
                    $('#EventModal #startDate').html(moment(calEvent.start).format('DD-MM-YYYY'));
                    $('#EventModal #endDate').html( function () {
                        if ( endDate1 == null) {
                            $('#EventModal #endDate').html(moment(calEvent.start).format('DD-MM-YYYY'));
                        }
                        else {
                            $('#EventModal #endDate').html(moment(calEvent.end).format('DD-MM-YYYY'));
                        }
                    });
                    $('#EventModal').show();
                },   
                customButtons: {
                    custom1: {
                        text: 'Franco',
                        click: function(clientEvents,calEvent,event) {
                            var elements_to_destroy = $("#calendar").fullCalendar('clientEvents')
                            for(i=0;i<elements_to_destroy.length;i++){
                                if (elements_to_destroy[i].benefit_id == 2) {
                                    var event_destroy =elements_to_destroy[i].id;
                                    $("#calendar").fullCalendar('removeEvents',event_destroy);
                                }
                                else {
                                    continue;
                                }   
                            }
                        }
                    },
                    custom2: {
                        text: 'Dia de Estudio',
                        click: function(clientEvents,calEvent,event) {
                            var elements_to_destroy = $("#calendar").fullCalendar('clientEvents')
                            for(i=0;i<elements_to_destroy.length;i++){
                                console.log(elements_to_destroy);
                                if (elements_to_destroy[i].benefit_id == 3) {

                                    var event_destroy =elements_to_destroy[i].id;
                                    $("#calendar").fullCalendar('removeEvents',event_destroy);
                                }
                                else {
                                    continue;
                                }   
                            }
                        }

                    },
                    custom3: {
                        text: 'Eliminar Filtros',
                        click: function(clientEvents,calEvent,event) {
                            $("#calendar").fullCalendar('refetchEvents');
                        }
                    },
                    custom4: {
                        text: 'Licencia Médica',
                        click: function(clientEvents,calEvent,event) {
                            var elements_to_destroy = $("#calendar").fullCalendar('clientEvents')
                            for(i=0;i<elements_to_destroy.length;i++){
                                if (elements_to_destroy[i].benefit_id == 4) {
                                    var event_destroy =elements_to_destroy[i].id;
                                    $("#calendar").fullCalendar('removeEvents',event_destroy);
                                }
                                else {
                                    continue;
                                }   
                            }
                        }

                    },
                    custom5: {
                        text: 'Vacaciones',
                        click: function(clientEvents,calEvent,event) {
                            var elements_to_destroy = $("#calendar").fullCalendar('clientEvents')
                            for(i=0;i<elements_to_destroy.length;i++){
                                if (elements_to_destroy[i].benefit_id == 1) {
                                    var event_destroy =elements_to_destroy[i].id;
                                    $("#calendar").fullCalendar('removeEvents',event_destroy);
                                }
                                else {
                                    continue;
                                }   
                            }
                        }

                    },
                    custom6: {
                        text: 'Cambio de Horario',
                        click: function(clientEvents,calEvent,event) {
                            var elements_to_destroy = $("#calendar").fullCalendar('clientEvents')
                            for(i=0;i<elements_to_destroy.length;i++){
                                if (elements_to_destroy[i].benefit_id == 5) {
                                    var event_destroy =elements_to_destroy[i].id;
                                    $("#calendar").fullCalendar('removeEvents',event_destroy);
                                }
                                else {
                                    continue;
                                }   
                            }
                        }

                    }
                },
                navLinks: true, 
                editable: false,
                eventLimit: false,
                height: 500,
                events: BASEURL + '/event',
                eventMouseover: function (data, event, view) {

                    tooltip = '<div class="tooltiptopicevent" style=" border-radius:5px;color:black;width:auto;height:auto;background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'Hace click para más información '+ '</div>';


                    $("body").append(tooltip);
                    $(this).mouseover(function (e) {
                        $(this).css('z-index', 10000);
                        $('.tooltiptopicevent').fadeIn('500');
                        $('.tooltiptopicevent').fadeTo('10', 1.9);
                    }).mousemove(function (e) {
                        $('.tooltiptopicevent').css('top', e.pageY + 10);
                        $('.tooltiptopicevent').css('left', e.pageX + 20);
                    });


                },
                eventMouseout: function (data, event, view) {
                    $(this).css('z-index', 8);

                    $('.tooltiptopicevent').remove();
                }       
            });
    }
    $(document).ready(function(){
        $.ajax("getRotaEmpleado").done(function(response){
            tipoRota =  {
                        id: response.id,
                        title: response.descripcion,
                        businessHours: {
                            
                                dow: [1, 2, 3, 4, 5 ], // Lunes, Martes, Miercoles, Jueves, Viernes
                                start: response.hora_inicio, 
                                end: response.hora_fin
                        }
                    }

            createCalendar(tipoRota);
        });
    });


</script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12"  >
            <div id="calendar" style="background-color: forestgreen; margin-top: 20px;"></div>
        </div>
    </div>
</div>

<div class="modal fade in" id="EventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="border-style: solid; border-radius: 5px; ">
         <div class="modal-content">
            <div class="modal-header" style="background-color: brown; color: white; text-align: center; border-bottom: solid 2px black;">
                  <h5 style="text-decoration: underline;text-transform: uppercase;"><strong>Información de Solicitud</strong></h5>
            </div> 
            <div class="modal-body" id="EventModalBody" style="height: 100px;">
                <div class=" col-md-12">
                    <div class="col-md-6">
                        <label for="event_title" class="col-2 col-form-label">Tipo de Licencia:</label>
                        <span name="event_title"  type="text" id="event_title"> </span>
                    </div>
                </div>
                <div class=" col-md-12">
                    <div class="col-md-6">
                        <label for="fecha_inicio" class="col-2 col-form-label">Fecha Inicio:</label>
                            <span name="fecha_inicio"  type="text" id="startDate"> </span>
                    </div>
                    <div class="col-md-6">
                        <label for="fecha_fin" class="col-2 col-form-label">Fecha Fin:</label>
                        <span name="fecha_fin"  type="text" id="endDate"> </span>
                    </div>
                </div>
            </div> 
            <div class="modal-footer" style="border-top:solid 1px black; background-color: whitesmoke;">
                <button class="btn btn-link btn-success" onclick='$("#EventModal").hide();' data-dismiss="modal" style="font-size:15px;color:blue;cursor: pointer;text-decoration: none;"><strong style="color:white;">Continuar navegando...</strong></button>
            </div>  
        </div>
    </div>
</div>

</body>
</html>