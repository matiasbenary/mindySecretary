{{ csrf_field() }}
    <div class=" col-xs-6">
        <label for="example-text-input" class="col-2 col-form-label">DNI</label>
        <div class="col-10">
            <input name="dni" class="form-control" type="number" value="{{$entity->dni}}" id="dni-input"  >
        </div>
        <label for="dni-input" lass="col-2 col-form-label">{{$errors->first('dni')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="example-search-input" class="col-2 col-form-label">Nombre</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="nombre" class="form-control" type="text" value="{{$entity->nombre}}" id="name-input" >
        </div>
        <label for="name-input" lass="col-2 col-form-label">{{$errors->first('name')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="example-search-input" class="col-2 col-form-label">Apellido</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="apellido" class="form-control" type="text" value="{{$entity->apellido}}" id="lastname-input" >
        </div>
        <label for="lastname-input" lass="col-2 col-form-label">{{$errors->first('lastname')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="birthday-input" class="col-2 col-form-label">Fecha nacimiento</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="fecha_nacimiento" class="form-control" type="date" value="{{$entity->fecha_nacimiento}}" id="birthday-input" >
        </div>
        <label for="birthday-input" lass="col-2 col-form-label">{{$errors->first('fecha_nacimiento')}}</label>
    </div>

    SECTOR SELECCIONABLE
    <div class=" col-xs-6">
        <label for="example-search-input" class="col-2 col-form-label">Antigüedad</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="antiguedad" class="form-control" type="number" value="{{$entity->antiguedad}}" id="antiguedad-code-input" required>
        </div>
        <label for="antiguedad-code-input" lass="col-2 col-form-label">{{$errors->first('antiguedad')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="email-input" class="col-2 col-form-label">Email</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="email" class="form-control" type="text" value="{{$entity->email}}" id="email-input" >
        </div>
        <label for="email-input" lass="col-2 col-form-label">{{$errors->first('email')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="bornplace-input" class="col-2 col-form-label">Lugar de nacimiento</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="lugar_nacimiento" class="form-control" type="text" value="{{$entity->lugar_nacimiento}}" id="bornplace-input" >
        </div>
        <label for="bornplace-input" lass="col-2 col-form-label">{{$errors->first('lugar_nacimiento')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="example-tel-input" class="col-2 col-form-label">Teléfono</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="telefono" class="form-control" type="number" value="{{$entity->telefono}}" id="tel-input" required>
        </div>
        <label for="tel-input" lass="col-2 col-form-label">{{$errors->first('telefono')}}</label>
    </div>

    <div class=" col-xs-6">
        <label class="mr-sm-2" for="inlineFormCustomSelect">Sexo</label>
        <select disabled="{{!$readOnlyForm}} name="sexo" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect" selected="{{$entity->sexo}}">
            <option value="H">Hombre</option>
            <option value="M">Mujer</option>
            <option value="O">Otro</option>
        </select>
    </div>

    <div class=" col-xs-6">
        <label class="mr-sm-2" for="inlineFormCustomSelect">Estado civil</label>
        <select disabled="{{!$readOnlyForm}} name="estado_civil" class="form-control custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect" selected="{{$entity->estado_civil}}">
            <option value="Casado">Casado</option>
            <option value="Soltero">Soltero</option>
            <option value="Viudo">Viudo</option>
            <option value="Otro">otro</option>
        </select>
    </div>

    <div class=" col-xs-6">
        <label for="example-search-input" class="col-2 col-form-label">Dirección</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="direccion" class="form-control" type="text" value="{{$entity->direccion}}" id="address-input" required>
        </div>
        <label for="address-input" lass="col-2 col-form-label">{{$errors->first('direccion')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="city-input" class="col-2 col-form-label">Ciudad</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="ciudad" class="form-control" type="text" value="{{$entity->ciudad}}" id="city-input" required>
        </div>
        <label for="city-input" lass="col-2 col-form-label">{{$errors->first('ciudad')}}</label>
    </div>

    <div class=" col-xs-6">
        <label for="provincia-input" class="col-2 col-form-label">Provincia</label>
        <div class="col-10">
            <input name="provincia" class="form-control" type="text" value="{{$entity->provincia}}" id="provincia-input" required>
        </div>
        <label for="provincia-input" lass="col-2 col-form-label">{{$errors->first('provincia')}}</label>
    </div>


    <div class=" col-xs-6">
        <label for="example-search-input" class="col-2 col-form-label">Codigo postal</label>
        <div class="col-10">
            <input disabled="{{!$readOnlyForm}} name="codigo_postal" class="form-control" type="number" value="{{$entity->codigo_postal}}" id="postal-code-input" required>
        </div>
        <label for="postal-code-input" lass="col-2 col-form-label">{{$errors->first('codigo_postal')}}</label>
    </div>
    <div class="col-xs-6">
        <button type="submit" class="btn btn-default" >Guardar</button>
    </div>
    <div class="col-xs-6">
        <button id="backButtonForm" onclick="backButton()" type="button" class="btn btn-default" > Volver</button>
    </div>

<script>
    function saveData(event,form){
        event.preventDefault();
        var data =new FormData();
        data = $(form).serializeArray();
        $.ajax({url:form.action,data:data,method:form.method}).done(function(response){
            $("#msg").html("Guardado exitosamente");
        }).fail(function(){
            $("#msg").text("No se pudieron guardar los cambios");
        }).always(function(){
            $("#msg").fadeIn();
            setTimeout(function(){$("#msg").fadeOut()},2000);
        });
    }
    function backButton(){
        window.history.back();
    }
</script>