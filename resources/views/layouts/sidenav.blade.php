<div id="menu_admin" class="nav-side-menu navbar-left col-xs-3" tabindex="0" >

    <div class="menu-list">
        
        <ul id="menu-content" class="menu-content collapse out">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('img/image_user_1.png')}}" class="img-circle" alt="user image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#">
                    <i class="fa fa-circle text-success"></i>
                    Online
                    </a>
                </div>
            </div>

            <li>
                <a href="/home" onclick="pepe()">
                    <i class="fa fa-dashboard fa-lg"></i> Dashboard
                </a>
            </li>

            <li  data-toggle="collapse" data-target="#products_admin" class="collapsed">
                <a href="#"><i class="fa fa-gift fa-lg"></i> Empleados <span class="arrow"></span></a>
            </li>
            <ul class="sub-menu collapse" id="products_admin">
                <li class="active"><a href="/employees_info">Todos</a></li>
                <li ><a href="/employees_info/create">Agregar nuevo</a></li>
            </ul>
             <li data-toggle="collapse" data-target="#sectores_group" class="collapsed">
                <a href="#">
                    <i class="fa fa-dashboard fa-lg"></i> Sectores<span class="arrow"></span>
                </a>
            </li>
             <ul class="sub-menu collapse" id="sectores_group">
                <li class="active"><a href="/sectors">Todos</a></li>
                <li ><a href="/sectors/create">Agregar nuevo</a></li>
            </ul>

        </ul>
    </div>
</div>

<div id="menu" class="nav-side-menu navbar-left col-xs-3" tabindex="0" >

    <div class="menu-list">
        
        <ul id="menu-content" class="menu-content collapse out">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('img/image_user_1.png')}}" class="img-circle" alt="user image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#">
                    <i class="fa fa-circle text-success"></i>
                    Online
                    </a>
                </div>
            </div>

            <li>
                <a href="/home">
                    <i class="fa fa-dashboard fa-lg"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="/calendar">
                    <i class="fa fa-dashboard fa-lg"></i> Calendario
                </a>
            </li>

        </ul>
    </div>
</div>



