@extends('layouts.app')

@section('content')

<input id="toggle-trigger" type="checkbox" data-toggle="toggle">
<div id="console-event"></div>
<button class="btn btn-success" onclick="toggleOn()">On by API</button>
<button class="btn btn-danger" onclick="toggleOff()">Off by API</button>
<button class="btn btn-success" onclick="toggleOnByInput()">On by Input</button>
<button class="btn btn-danger" onclick="toggleOffByInput()">Off by Input</button>
<script>
  function toggleOn() {
  	$('#toggle-trigger').bootstrapToggle('on')  
  }
  function toggleOff() {
    $('#toggle-trigger').bootstrapToggle('off')  
  }
  function toggleOnByInput() {
    $('#toggle-trigger').prop('checked', true).change()
  }
  function toggleOffByInput() {
    $('#toggle-trigger').prop('checked', false).change()
  }
  $(function() {
  	$('#toggle-trigger').change(function() {
    	$('#console-event').html('Toggle: ' + $(this).prop('checked'))
    })
   })

</script>

@endsection