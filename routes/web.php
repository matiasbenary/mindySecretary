<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function () {
	if (Auth::check()) {
        return redirect('/home');
    }
	else {
        return view('auth/login');
    }
});

Route::resource('/userRequestForm', 'RequestsController');
Route::post('sendUserRequest','RequestsController@store');

Route::get('getCompanyRequestsCount','RequestsController@getCompanyRequestsCount');

Route::get('getCompanyRequestStatusCount','RequestsController@getCompanyRequestStatusCount');


Route::get('/calendar', function() {
	 $sector = \App\Models\Sector::find(Auth::user()->employee->id_sector);
                           
            return view('calendar',['sector' => $sector]);

});


Route::get('/home', 'HomeController@index');
Route::post('/home', 'HomeController@role_switch');


Route::get('userData',function() {
    return View::make('User/userdata');
});
Route::get('userDatajs',function() {
    return View::make('User/userdatawithjs');
});

Route::post('userController','User\UserDataController@createUserData');

Route::get('deleteUserData','User\UserDataController@deleteUSerData');


Route::resource('userdata','User\UserdataController');

Route::resource('employees_info','EmployeesController');
Route::get('employees_info/{user_id}/detail','EmployeesController@detail');

Route::get('getRotaEmpleado','EmployeesController@getRotaEmpleado');

Route::get('getNotifications','EmployeesController@getNotifications');

Route::get('birthdaysTodayListHome','EmployeesController@getBirthDays2');

Route::get('birthdays/{month}','EmployeesController@getBirthDays');
Route::get('birthdays',function (){return View('Employees/birthdaysList');});
Route::get('events_list',function (){return View('Employees/events_list');});
//Route::get('sectors_list',function (){return View('sectorlist');});

Route::get('test','EmployeesController@getNotifications');
Route::get('/event', 'EventController@index');
Route::get('/eventForCalendar', 'EventController@eventForCalendar');
Route::get('/event_all', 'EventController@event_all');
Route::resource('/sectors', 'SectorController');

//Route::get('request',)